<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Series extends Model
{
    protected $_fields = [
        'b.bookID',
        'b.name',
        'b.size',
        'b.numberOfPages',
        'b.copyright',
        'b.ISBN',
        'b.grade',
        'ass.assetURL as imageUrl',
        'b.authors',
        's.seriesID'
    ];

    public function getProducts()
    {
        $series = DB::table('series AS s')
            ->join('series_asset_match AS sam', 's.seriesID', '=', 'sam.seriesID')
            ->join('asset AS ass', 'sam.assetID', '=', 'ass.assetID')
            ->select('s.seriesID', 's.description', 's.hasContentDiagram', 'ass.assetURL as imageUrl')
            ->get();

        foreach($series as $key => $data) {
            $data->books = $this->_getSeriesBooks($data->seriesID);
        }

        return $series;
    }

    public function getBooks()
    {
        return $this->_getSeriesBooks();
    }

    public function bookDetails($bookID)
    {
        $book = $this->_getSeriesBooks(null, $bookID);
        return $book;
    }


    protected function _getSeriesDescription($seriesID)
    {
        return DB::table('series AS s')
            ->select('s.description')
            ->where('s.seriesID', '=', $seriesID)
            ->get();
    }

    /*
     * @TODO: handle products with multiple images
     * @TODO: Redesign book authors in DB
     */
    protected function _getSeriesBooks($seriesID = null, $bookID = null)
    {
        $books = DB::table('books as b')
            ->leftjoin('book_asset_match AS bam', function ($join) {
                $join->on('b.bookID', '=', 'bam.bookID');
            })
            ->leftjoin('asset AS ass', 'bam.assetID', '=', 'ass.assetID')
            ->select(
                    'b.bookID', 'b.size', 'b.numberOfPages',
                    'b.copyright', 'b.ISBN', 'b.grade',
                    'ass.assetURL as imageUrl', 'b.mainAuthor');

        if (!empty($seriesID)) {
            $books->where('seriesID', $seriesID)
                ->where('bam.isLarge', '=', 0);
        }  else if (!empty($bookID)) {
            $books
                ->leftjoin('series as s', function ($join) {
                    $join->on('b.seriesID', '=', 's.seriesID');
                })
                ->select(
                        'b.bookID', 'b.name', 'b.size',
                        'b.numberOfPages', 'b.copyright', 'b.ISBN',
                        'b.grade', 'ass.assetURL as imageUrl', 'b.authors',
                        's.description', 's.hasContentDiagram', 'b.mainAuthor')
                ->where('b.bookID', $bookID)
                ->where('bam.isLarge', '=', 1);
        } else {
            $books->where('bam.isLarge', '=', 0);
        }

        $books->orderBy('b.bookID');
        return $books->get();
    }
}
