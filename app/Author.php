<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Author extends Model
{
    protected $table = 'authors';

    public function AuthorAssetMatch()
    {
        return $this->hasOne('App\models\AuthorAssetMatch', 'authorID');
    }

    public function getAuthors($authorsPage = false)
    {
        $response = DB::table('authors AS a')
            ->join('author_asset_match AS aam', 'a.authorID', '=', 'aam.authorID')
            ->join('asset AS ass', 'aam.assetID', '=', 'ass.assetID');

        if ($authorsPage) {
            $response
                ->select('a.authorID', 'a.firstName', 'a.middleName', 'a.lastName', 'a.fullDescription', 'ass.assetURL as imageUrl');
        } else {
            $response
                ->select('a.authorID', 'a.firstName', 'a.middleName', 'a.lastName', 'a.description', 'ass.assetURL as imageUrl');
        }

        return $response->get();
    }
}
