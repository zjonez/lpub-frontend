<?php

namespace App\models;

use Illuminate\Database\Eloquent\Model;

class AuthorAssetMatch extends Model
{
    protected $table = "author_asset_match";

    public function AssetMatch()
    {
        return $this->belongsTo('App\models\Asset.php');
    }
}
