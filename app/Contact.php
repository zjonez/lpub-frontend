<?php

namespace App;

use Mail;

use Illuminate\Database\Eloquent\Model;

class Contact extends Model
{
    const FROM_EMAIL = 'inquire@learnedpublishing.com.ph';
    const FROM_NAME = 'LPUB Inquiry';
    const TEST_EMAIL = 'jonas.zapanta@gmail.com';

    //@TODO: there should be a confirmation on the user's end
    public function sendInquiry($params)
    {
        $title = '[LPUB INQUIRY]: '.$params['subject'];
        $content = $params['message'];

        $success = 1;
        $message = null;

        try {
            Mail::send('emails.send', ['title' => $title, 'content' => $content, 'email' => $params['email'], 'name' => $params['name']], function ($message) use ($title) {
                $message->subject($title);
                $message->from(self::FROM_EMAIL, self::FROM_NAME);
                $message->to(self::TEST_EMAIL);
            });
            $message = "Inquiry has been sent";
        } catch (Exception $e) {
            $success = 0;
            $message = $e->getMessage();
        }

        return [
            'success' => $success,
            'message' => $message
        ];
    }
}
