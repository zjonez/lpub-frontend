<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function getCategories()
    {
        return DB::table('posts as p')
                ->join('category as c', 'c.categoryID', '=', 'p.categoryID')
                ->select('c.categoryID', 'c.name', DB::raw('count(*) as postCount'))
                ->groupBy('c.categoryID')
                ->get();
    }
}
