<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;

class AlphaMiddleWare
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        $isAlpha = env('APP_ALPHA');

        if ($isAlpha && empty(Auth::user())) {
            return Redirect::to('login');
        }

        return $next($request);
    }
}
