<?php

namespace App\Http\Controllers;

use App\Series;
use App\Http\Controllers\Controller;

//SEO Facades
use SEOMeta;
use OpenGraph;
use Twitter;

class ProductsController extends Controller
{
    public function index()
    {
        SEOMeta::setTitle('Learned Publishing - K to 12 Science Textbooks');
        SEOMeta::setDescription('Learned Publishing is the publisher of Science Impact K to 12 series for grades 1 to 10 edited by Delfin C. Bautista and is the official distributor of Academe Publishing House Inc.\'s textbooks.');
        SEOMeta::setCanonical('http://learnedpublishing.com.ph/products');

        return view('products');
    }

    public function series()
    {
        $series = new Series();

        return json_encode([
            'series' => $series->getProducts()
        ]);
    }

    public function books()
    {
        $series = new Series();

        return json_encode([
            'books' => $series->getBooks()
        ]);
    }

    public function details($bookID)
    {
        SEOMeta::setTitle('Learned Publishing - K to 12 Science Textbooks');
        SEOMeta::setDescription('Learned Publishing is the publisher of Science Impact K to 12 series for grades 1 to 10 edited by Delfin C. Bautista and is the official distributor of Academe Publishing House Inc.\'s textbooks.');

        $series = new Series();
        return view('product')->with('details', $series->bookDetails($bookID));
    }
}