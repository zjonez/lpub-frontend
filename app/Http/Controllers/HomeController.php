<?php

namespace App\Http\Controllers;

use App\Author;
use App\Contact;
use App\Http\Controllers\Controller;

//@TODO: consolidate common facades
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Support\Facades\Validator;

//SEO Facades
use SEOMeta;
use OpenGraph;
use Twitter;

class HomeController extends Controller
{
    public function index()
    {
        SEOMeta::setTitle('Learned Publishing - Philippine Textbook Publisher');
        SEOMeta::setDescription('Learned Publishing is the publisher of Science Impact series edited by Delfin C. Bautista (Ateneo) and the official distributor of Academe Publishing\'s textbooks.');
        SEOMeta::setCanonical('http://learnedpublishing.com.ph');

        OpenGraph::setDescription('Learned Publishing is the publisher of Science Impact series edited by Delfin C. Bautista (Ateneo) and the official distributor of Academe Publishing\'s textbooks.');
        OpenGraph::setTitle('Learned Publishing - Philippine Textbook Publisher');
        OpenGraph::setUrl('http://www.learnedpublishing.com.ph');

        OpenGraph::addImage('http://www.learnedpublishing.com.ph/images/lp-logo.jpg', ['height' => 300, 'width' => 300]);

        return view('home');
    }

    public function about()
    {
        SEOMeta::setTitle('Learned Publishing - Company Profile and Authors / Editors');
        SEOMeta::setDescription('Learned Publishing\'s company profile and authors / editors from Philippine Science High School, Ateneo de Manila University, and University of The Philippines.');
        SEOMeta::setCanonical('http://learnedpublishing.com.ph/about-us');

        return view('about');
    }

    public function authors()
    {
        SEOMeta::setTitle('Learned Publishing - Author\'s / Editor\'s Profiles');
        SEOMeta::setDescription('Learned Publishing\'s list of authors and editors, with Delfin C. Bautista as the overall Science Coordinator and consultant');
        SEOMeta::setCanonical('http://learnedpublishing.com.ph/authors');

        $author = new Author();
        return view('authors')->with('authors', $author->getAuthors(true));
    }

    public function showLogin()
    {
        return view('login');
    }

    public function doLogin()
    {
        // validate the info, create rules for the inputs
        $rules = [
            'email'    => 'required|email', // make sure the email is an actual email
            'password' => 'required|alphaNum|min:3' // password can only be alphanumeric and has to be greater than 3 characters
        ];

        // run the validation rules on the inputs from the form
        $validator = Validator::make(Input::all(), $rules);

        // if the validator fails, redirect back to the form
        if ($validator->fails()) {
            return Redirect::to('login')
                ->withErrors($validator) // send back all errors to the login form
                ->withInput(Input::except('password')); // send back the input (not the password) so that we can repopulate the form
        } else {
            // create our user data for the authentication
            $userdata = array(
                'email'     => Input::get('email'),
                'password'  => Input::get('password')
            );

            // attempt to do the login
            if (Auth::attempt($userdata)) {

                $user = Auth::user();
                return Redirect::to('/');

            } else {
                // user does not exist
                // validation not successful, send back to form
                return Redirect::to('login');

            }
        }
    }

    public function contact()
    {
        SEOMeta::setTitle('Learned Publishing - Contact Details');
        SEOMeta::setDescription('Contact Learned Publishing, office located at Antipolo City near S.M. Masinag');
        SEOMeta::setCanonical('http://learnedpublishing.com.ph/contact-us');

        return view('contact');
    }

    public function doContact()
    {
        $params = Input::all();
        $contact = new Contact();
        $response = $contact->sendInquiry($params);
        return response()->json($response);
    }
}