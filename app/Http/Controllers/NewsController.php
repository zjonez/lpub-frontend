<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Category;
use App\Posts;

use Illuminate\Support\Facades\Input;
use App\Http\Requests;

use Illuminate\Support\Facades\Auth;
//use DB;

//SEO Facades
use SEOMeta;
use OpenGraph;
use Twitter;

class NewsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\View\View
     */
    public function index()
    {
        SEOMeta::setTitle('Learned Publishing - News and Training Schedules');
        SEOMeta::setDescription('Get the latest updates from Learned Publishing, science articles, new textbooks, and teacher training schedules');
        SEOMeta::setCanonical('http://learnedpublishing.com.ph/news');

        $post = new Posts();
        $category = new Category();
        $settings = [
            'latestPosts' => $post->getPosts(null, Posts::PAGE_SIZE, true),
            'categories' => $category->getCategories(),
            'page' => 1,
            'user' => Auth::user()
        ];
//        var_dump(DB::getQueryLog());
        return view('news')->with('settings', json_encode($settings, JSON_NUMERIC_CHECK));
    }

    public function get()
    {
        $posts = new Posts();
        $page = Input::get("page");

        return response()->json([
            'posts' => $posts->getPosts(null, Posts::PAGE_SIZE, false, $page),
            'count' => $posts->getCount()
        ]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\View\View
     */
    public function create()
    {
        $category = new Category();

        $settings = [
            'categories' => $category->getCategories(),
            'user' => Auth::user()
        ];

        return view('news.create')->with('settings', json_encode($settings, JSON_NUMERIC_CHECK));
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\View\View
     */
    public function show($id)
    {
        $post = new Posts();
        $category = new Category();

        $settings = [
            'latestPosts' => $post->getPosts(null, 3, true),
            'categories' => $category->getCategories(),
        ];

        return view('single-news')
            ->with('settings', json_encode($settings, JSON_NUMERIC_CHECK))
            ->with('details', $post->getPosts($id));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
