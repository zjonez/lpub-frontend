<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;

//SEO Facades
use SEOMeta;
use OpenGraph;
use Twitter;

class ServicesController extends Controller
{
    public function index()
    {
        SEOMeta::setTitle('Learned Publishing - Science Teacher Training');
        SEOMeta::setDescription('Learned Publishing offers a unique and exclusive training for science teachers by using improvised science instruments of Scinergy Enterprises.');
        SEOMeta::setCanonical('http://learnedpublishing.com.ph/services');

        return view('services');
    }
}