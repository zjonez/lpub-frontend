<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|s
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/

//Middleware routes (Alpha test)
Route::group(['middleware' => 'alpha'], function () {
    Route::get('/', 'HomeController@index');

    Route::get('/about-us', 'HomeController@about');

    Route::get('/products', 'ProductsController@index');

    Route::get('/services', 'ServicesController@index');

    Route::get('/series', 'ProductsController@series');

    Route::get('/books', 'ProductsController@books');

    Route::get('/book/{id}', 'ProductsController@details');

    Route::get('/authors', 'HomeController@authors');

    Route::get('/news', 'NewsController@index');

    Route::get('/news/{id}', 'NewsController@show');

    Route::get('/contact-us', 'HomeController@contact');

    Route::post('/contact-us', 'HomeController@doContact');

    //REST
    Route::resource('get-authors', 'AuthorController');

    Route::resource('/get-posts', 'NewsController@get');

    Route::resource('/new-post', 'NewsController@create');
});

//login module
Route::get('login', array('uses' => 'HomeController@showLogin'));
Route::post('login', array('uses' => 'HomeController@doLogin'));
Route::get('test-login', array('uses' => 'HomeController@testLogin'));

/*
    test route
*/
    Route::get('/test', function () {
        return view('test');
    });