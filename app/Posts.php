<?php

namespace App;

use DB;
use Illuminate\Database\Eloquent\Model;

class Posts extends Model
{
    const PAGE_SIZE = 3;

    public function getPosts($id = null, $pageSize = self::PAGE_SIZE, $isLatest = false, $page = 1)
    {
        $page = $page - 1;

        $response = DB::table('posts AS p')
            ->join('category AS c', 'c.categoryID', '=', 'p.categoryID');

        if ($isLatest) {
            $response->select(
                'p.postID', 'p.title', 'p.thumbnailURL',
                'p.username', 'p.created_at'
            );
        } else {
            $response->select(
                'p.postID', 'p.title', 'p.content',
                'p.mainImage', 'p.username', 'p.created_at',
                'c.name AS categoryName'
            );
        }

        if ($id) {
            $response
                ->where('p.postID', '=', $id);
        }

        return
            $response
                ->orderBy('p.created_at', 'desc')
                ->take($pageSize)
                ->skip($page)
        ->get();
    }

    public function getCount()
    {
        return DB::table('posts')->count();
    }
}
