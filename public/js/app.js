
var app = angular.module('app', [
    'angularSpinner',
    'restangular',
    'ui.bootstrap',
    'bootstrapLightbox'
]);

app.constant('SETTINGS', SETTINGS);

app.config(function ($interpolateProvider) {
        $interpolateProvider.startSymbol('<%');
        $interpolateProvider.endSymbol('%>');
});

//@TODO: check if this can be in a separate file
app.controller('mainController', [
        '$scope',
        function ($scope) {
                //@TODO: quotes section, make this dynamic(DB Driven). Also create a service for this
                $scope.quotes = [
                    {
                        text: "Tell me and I'll forget. Show me and I may remember. Involve me and I learn.",
                        origin: "Benjamin Franklin"
                    },
                    {
                        text: "I cannot teach anybody anything, I can only make them think.",
                        origin: "Socrates"
                    },
                    {
                        text: "Education is not a preparation for life; education is life itself.",
                        origin: "John Dewey"
                    },
                    {
                        text: "Education isn't something you can finish.",
                        origin: "Isaac Asimov"
                    },
                    {
                        text: "The only thing more expensive than education is ignorance.",
                        origin: "Benjamin Franklin"
                    },
                    {
                        text: "If you can't explain it simply, you don't understand it well enough.",
                        origin: "Albert Einstein"
                    },
                    {
                        text: "The best teachers are those who show you where to look, but don't tell you what to see.",
                        origin: "Alexandra K. Trenfor"
                    },
                    {
                        text: "Education is a progressive discovery of our own ignorance.",
                        origin: "Will Durant"
                    },
                    {
                        text: "Real knowledge is to know the extent of one's ignorance.",
                        origin: "Confucius"
                    },
                    {
                        text: "In the age of information, ignorance is a choice.",
                        origin: "Donny Miller"
                    }
                ];

                $scope.quote = $scope.quotes[Math.floor(Math.random() * $scope.quotes.length)];
        }
]);

app.filter('sanitize', function ($sce) {
        return function (ss) {
                return $sce.trustAsHtml(ss)
        };
});

app.filter('gradeLabel', function () {
    return function (input) {
        return "Grade "+input;
    };
});

app.filter('ellipsis', function () {
    return function (input, number) {
        return input.slice(0, number) + '...';
    }
});

app.filter('myDateFormat', function myDateFormat($filter) {
    return function (text, format) {
        var tempdate = new Date(text.replace(/-/g, "/"));
        return $filter('date')(tempdate, format);
    }
});

$(document).ready(function () {
        $('.top-button a').click(function () {
                $(window).scrollTop(0);
        });
});

//# sourceMappingURL=app.js.map
