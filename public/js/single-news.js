app
    .controller('singleNewsController', [
        'SETTINGS',
        '$scope',
        'usSpinnerService',
        'postService',
        '$timeout',
        function (
            SETTINGS,
            $scope,
            usSpinnerService,
            postService,
            $timeout
        ) {
            $scope.settings = JSON.parse(SETTINGS);
        }
    ]);
//# sourceMappingURL=single-news.js.map
