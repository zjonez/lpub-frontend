app
    .service('authorService', ['$q', 'Restangular',
        function ($q, Restangular) {
            var Service = {};

            var authorsRest = Restangular.all('get-authors');

            Service.getAuthors = function() {
                var deferred = $q.defer();
                deferred.resolve(authorsRest.customGET(""));
                return deferred.promise;
            };

            return Service;
        }
    ])
;
//# sourceMappingURL=authors.js.map
