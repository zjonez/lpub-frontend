app
    .service('imageService', ['$q', 'Restangular',
        function ($q, Restangular) {
            var Service = {};
            var data = {};

            data.images = [
                {
                    imageUrl: 'images/uploads/slider-images/slider01.jpg',
                    sequence: 1
                },
                {
                    imageUrl: 'images/uploads/slider-images/slider02.jpg',
                    sequence: 2
                },
                {
                    imageUrl: 'images/uploads/slider-images/slider03.jpg',
                    sequence: 3
                }
            ];

            data.gallery = [
                {
                    caption: 'Taken from Starland International School',
                    imageUrl: 'images/uploads/gallery/instrumentation1.jpg',
                    thumbnailUrl: 'images/uploads/gallery/small/instrument1-small.jpg'
                },
                {
                    caption: 'Taken from Starland International School',
                    imageUrl: 'images/uploads/gallery/instrumentation2.jpg',
                    thumbnailUrl: 'images/uploads/gallery/small/instrument2-small.jpg'
                },
                {
                    caption: 'Taken from Starland International School',
                    imageUrl: 'images/uploads/gallery/instrumentation3.jpg',
                    thumbnailUrl: 'images/uploads/gallery/small/instrument3-small.jpg'
                },
                {
                    caption: 'Taken from Starland International School',
                    imageUrl: 'images/uploads/gallery/instrumentation4.jpg',
                    thumbnailUrl: 'images/uploads/gallery/small/instrument4-small.jpg'
                },
                {
                    caption: 'Taken from Spark School Pasig',
                    imageUrl: 'images/uploads/gallery/spark-school-pasig-edit.jpg',
                    thumbnailUrl: 'images/uploads/gallery/small/instrument5-small.jpg'
                },
                {
                    caption: 'Taken from St. Vincent Learning Center',
                    imageUrl: 'images/uploads/gallery/svlc1.jpg',
                    thumbnailUrl: 'images/uploads/gallery/small/instrument6-small.jpg'
                },
                {
                    caption: 'Taken from St. Vincent Learning Center',
                    imageUrl: 'images/uploads/gallery/svlc2.jpg'
                },
                {
                    caption: 'Taken from Starland International School',
                    imageUrl: 'images/uploads/gallery/instrumentation1.jpg',
                    thumbnailUrl: 'images/uploads/gallery/small/instrument1-small.jpg'
                }
            ];

            //@TODO: this should be DB Driven
            Service.getSliderImages = function () {
                var deferred = $q.defer();
                deferred.resolve(data);
                return deferred.promise;
            };

            Service.getGalleryImages = function () {
                var deferred = $q.defer();
                deferred.resolve(data);
                return deferred.promise;
            };

            return Service;
        }
    ])
;
//# sourceMappingURL=images.js.map
