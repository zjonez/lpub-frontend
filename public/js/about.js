app
    .controller('aboutController', [
        '$scope',
        'usSpinnerService',
        'authorService',
        '$timeout',
        function ($scope,
                  usSpinnerService,
                  authorService,
                  $timeout) {

            $scope.isLoading = false;

            $scope.getAuthors = function () {
                $scope.isLoading = true;
                $timeout(function () {
                    usSpinnerService.spin('spinner-1');
                }, 500);


                authorService
                    .getAuthors()
                    .then(function (response) {
                        $scope.authors = response.authors;
                        $scope.isLoading = false;
                        $timeout(function () {
                            usSpinnerService.stop('spinner-1');
                        }, 500);
                    });
            };

            $scope.init = function () {
                $scope.getAuthors();
            };

            $scope.init();

        }])

    .filter('authorName', function () {
        return function (input) {
            return input.firstName + ' ' + input.middleName.charAt(0) + ". " + input.lastName;
        }
    })
;
//# sourceMappingURL=about.js.map
