app
    .controller('homeController', [
        '$scope',
        'usSpinnerService',
        'imageService',
        'bookService',
        '$timeout',
        function(
            $scope,
            usSpinnerService,
            imageService,
            bookService,
            $timeout
        ) {
            $scope.isLoading = false;
            $scope.productsLoading = false;

            $scope.sliderStop = false;

            $scope.loadSliderImages = function () {
                $timeout(function () {
                    usSpinnerService.spin('spinner-1');
                }, 500);

                imageService
                    .getSliderImages()
                    .then(function (response) {
                        $scope.isLoading = false;
                        $timeout(function () {
                            usSpinnerService.stop('spinner-1');
                        }, 500);

                        $scope.images = response.images;

                        $('#homeCarousel').carousel({
                            interval: 4000
                        });

                        $('#homeCarousel').on('slid.bs.carousel', function () {
                        });
                    });
            };

            $scope.loadBooks = function () {
                $scope.productsLoading = true;
                $timeout(function () {
                    usSpinnerService.spin('spinner-2');
                }, 500);

                bookService
                    .getBooks()
                    .then(function (response) {
                        var results = response.books;
                        var pages = Math.ceil(results.length / 3);

                        //LOOP FUNCTION
                        var totalBooks = pages * Math.round(results.length / pages);
                        if(totalBooks > results.length) {
                            var difference = totalBooks - results.length;
                            var i = 0;
                            _.each(_.range(difference), function(){
                                results.push(results[i]);
                                i++;
                            })
                        }

                        $scope.bookPages = _.range(pages);
                        $scope.bookValues = $scope.updatedMapValues(results, pages, 3);
                        $scope.productsLoading = false;
                        $timeout(function () {
                            usSpinnerService.stop('spinner-2');
                        }, 500);

                        $('#productsCarousel').carousel({
                            interval: 2000,
                            pause: "false"
                        });

                        $('#productsCarousel').on('slid.bs.carousel', function () {
                        });

                    });
            };

            $scope.updatedMapValues = function (results, pageLength, pageSize) {
                var ctr = 0;
                var values = [];
                var pages = _.range(pageLength);
                var imagesPerPage = _.range(pageSize);

                _.each(pages, function (i) {
                    values.push({page: i});
                    values[i].items = [];
                    _.each(imagesPerPage, function (value, key) {
                        if (!_.isUndefined(results[ctr])) {
                            values[i].items.push(results[ctr]);
                        }
                        ctr = ctr + 1;
                    });
                });

                return values;
            };

            $scope.pauseSlider = function() {
                $scope.sliderStop = true;
                $('#productsCarousel').carousel('pause');
            };

            $scope.playSlider = function() {
                $scope.sliderStop = false;
                $('#productsCarousel').carousel('cycle');
            };

            $scope.init = function () {
                $scope.loadSliderImages();
                $scope.loadBooks();
            };

            $scope.init();
        }
    ])
;



//# sourceMappingURL=home.js.map
