<?php

use Illuminate\Database\Seeder;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'name' => 'Juancho Tupas',
            'email' => 'tupas.juancho@gmail.com',
            'password' => bcrypt('jambalaya'),
        ]);
    }
}
