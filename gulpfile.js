var elixir = require('laravel-elixir');

/*
 |--------------------------------------------------------------------------
 | Elixir Asset Management
 |--------------------------------------------------------------------------
 |
 | Elixir provides a clean, fluent API for defining some basic Gulp tasks
 | for your Laravel application. By default, we are compiling the Sass
 | file for our application, as well as publishing vendor resources.
 |
 */

elixir(function (mix) {
    
    //less
    //gulp less
    mix
        .less('vars.less')
        .less('styles.less')
        .less('home.less')
        .less('about.less')
        .less('products.less')
        .less('services.less')
        .less('single-product.less')
        .less('news.less')
        .less('contact.less')
    ;

    //Bower Components
    //run gulp copy
    mix
        //css
        .copy('bower_components/hover/css/hover-min.css', 'public/css/vendor/hover-min.css')
        .copy('bower_components/angular-bootstrap-lightbox/dist/angular-bootstrap-lightbox.min.css', 'public/css/vendor/angular-bootstrap-lightbox.min.css')

        //scripts
        .copy('bower_components/angular/angular.min.js','public/js/vendor/angular.min.js')
        .copy('bower_components/spin.js/spin.js','public/js/vendor/spin.js')
        .copy('bower_components/angular-spinner/angular-spinner.min.js','public/js/vendor/angular-spinner.min.js')
        .copy('bower_components/restangular/dist/restangular.min.js', 'public/js/vendor/restangular.min.js')
        .copy('bower_components/angular-animate/angular-animate.min.js', 'public/js/vendor/angular-animate.min.js')
        .copy('bower_components/angular-bootstrap/ui-bootstrap-tpls.min.js', 'public/js/vendor/ui-bootstrap-tpls.min.js')
        .copy('bower_components/angular-bootstrap-lightbox/dist/angular-bootstrap-lightbox.min.js', 'public/js/vendor/angular-bootstrap-lightbox.min.js')
    ;

    // Application Scripts
    //run gulp scripts
    mix
        .scripts([
            '../../../resources/scripts/app.js'
        ], 'public/js/app.js')
        .scripts([
            '../../../resources/scripts/home.js'
        ], 'public/js/home.js')

        .scripts([
            '../../../resources/scripts/about.js'
        ], 'public/js/about.js')

        .scripts([
            '../../../resources/scripts/services.js'
        ], 'public/js/services.js')

        .scripts([
            '../../../resources/scripts/products.js'
        ], 'public/js/products.js')

        .scripts([
            '../../../resources/scripts/single-product.js'
        ], 'public/js/single-product.js')

        .scripts([
            '../../../resources/scripts/news.js'
        ], 'public/js/news.js')

        .scripts([
            '../../../resources/scripts/single-news.js'
        ], 'public/js/single-news.js')

        .scripts([
            '../../../resources/scripts/contact.js'
        ], 'public/js/contact.js')

        //services

        .scripts([
            '../../../resources/scripts/service/authors.js'
        ], 'public/js/service/authors.js')

        .scripts([
            '../../../resources/scripts/service/images.js'
        ], 'public/js/service/images.js')

        .scripts([
            '../../../resources/scripts/service/instruments.js'
        ], 'public/js/service/instruments.js')

        .scripts([
            '../../../resources/scripts/service/books.js'
        ], 'public/js/service/books.js')

        .scripts([
            '../../../resources/scripts/service/post.js'
        ], 'public/js/service/post.js')
    ;
});
