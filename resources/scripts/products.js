app
    .controller('productsController', [
        '$scope',
        'bookService',
        'usSpinnerService',
        '$timeout',
        function(
                $scope,
                bookService,
                usSpinnerService,
                $timeout
        ) {
            $scope.isLoading = false;

            $scope.loadSeries = function () {
                $scope.isLoading = true;
                $timeout(function () {
                    usSpinnerService.spin('spinner-1');
                }, 500);

                bookService
                    .getSeries()
                    .then(function (response) {
                        $scope.series = response.series;

                        _.forEach($scope.series, function (data) {
                            var pages = Math.ceil(data.books.length / 3);
                            data.bookPages = _.range(pages);

                            //LOOP FUNCTION
                            var totalBooks = pages * Math.round(data.books.length / pages);
                            if (totalBooks > data.books.length) {
                                var difference = totalBooks - data.books.length;
                                var i = 0;
                                _.each(_.range(difference), function () {
                                    data.books.push(data.books[i]);
                                    i++;
                                })
                            }

                            data.bookValues = $scope.updatedMapValues(data.books, pages, 3);
                        });

                        $scope.isLoading = false;
                        $timeout(function () {
                            usSpinnerService.stop('spinner-1');
                            $scope.initializeProductSliders();
                        }, 500);
                    });
            };

            $scope.updatedMapValues = function (results, pageLength, pageSize) {
                var ctr = 0;
                var values = [];
                var pages = _.range(pageLength);
                var itemsPerPage = _.range(pageSize);

                _.each(pages, function (i) {
                    values.push({page: i});
                    values[i].items = [];
                    _.each(itemsPerPage, function (value, key) {
                        if (!_.isUndefined(results[ctr])) {
                            values[i].items.push(results[ctr]);
                        }
                        ctr = ctr + 1;
                    });
                });

                return values;
            };

            $scope.initializeProductSliders = function () {
                var sliders = _.range(2);

                _.forEach(sliders, function (data, key) {
                    $timeout(function () {
                        $('#productsCarousel_' + key).carousel({
                            interval: 4000,
                            pause: "false"
                        });

                        $('#productsCarousel_' + key).on('slid.bs.carousel', function () {
                        });
                    }, 500);
                });
            };

            $scope.pauseSlider = function(data, index) {
                data.sliderStop = true;
                $('#productsCarousel_'+index).carousel('pause');
            };

            $scope.playSlider = function(data, index) {
                data.sliderStop = false;
                $('#productsCarousel_'+index).carousel('cycle');
            };

            $scope.init = function() {
                $scope.loadSeries();
            };

            $scope.init();
        }
    ]);