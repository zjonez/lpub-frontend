app
    .controller('singleProductController', [
        '$scope',
        'bookService',
        'usSpinnerService',
        '$timeout',
        'Lightbox',
        function (
                $scope,
                bookService,
                usSpinnerService,
                $timeout,
                Lightbox
        ) {

            $scope.loadBooks = function () {
                $scope.productsLoading = true;
                $timeout(function () {
                    usSpinnerService.spin('spinner-1');
                }, 500);

                bookService
                    .getBooks()
                    .then(function (response) {
                        var results = response.books;
                        var pages = Math.ceil(results.length / 3);
                        $scope.bookPages = _.range(pages);

                        //LOOP FUNCTION
                        var totalBooks = pages * Math.round(results.length / pages);
                        if(totalBooks > results.length) {
                            var difference = totalBooks - results.length;
                            var i = 0;
                            _.each(_.range(difference), function(){
                                results.push(results[i]);
                                i++;
                            })
                        }

                        $scope.bookValues = $scope.updatedMapValues(results, pages, 3);
                        $scope.productsLoading = false;
                        $timeout(function () {
                            usSpinnerService.stop('spinner-1');
                            $scope.loadSlider();
                        }, 500);
                    });
            };

            $scope.updatedMapValues = function (results, pageLength, pageSize) {
                var ctr = 0;
                var values = [];
                var pages = _.range(pageLength);
                var imagesPerPage = _.range(pageSize);

                _.each(pages, function (i) {
                    values.push({page: i});
                    values[i].items = [];
                    _.each(imagesPerPage, function (value, key) {
                        if (!_.isUndefined(results[ctr])) {
                            values[i].items.push(results[ctr]);
                        }
                        ctr = ctr + 1;
                    });
                });

                return values;
            };

            $scope.loadSlider = function () {
                $('#productsCarousel').carousel({
                    interval: 2000,
                    pause: "false"
                });

                $('#productsCarousel').on('slid.bs.carousel', function () {
                });
            };

            $scope.pauseSlider = function() {
                $scope.sliderStop = true;
                $('#productsCarousel').carousel('pause');
            };

            $scope.playSlider = function() {
                $scope.sliderStop = false;
                $('#productsCarousel').carousel('cycle');
            };

            $scope.init = function () {
                //@TODO: return all books except selected
                $scope.loadBooks();
            };

            $scope.init();
        }
    ]);