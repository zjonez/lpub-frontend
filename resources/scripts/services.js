app
    .controller('servicesController', [
        '$scope',
        'instrumentsService',
        'imageService',
        'usSpinnerService',
        '$timeout',
        'Lightbox',
        function ($scope,
                  instrumentsService,
                  imageService,
                  usSpinnerService,
                  $timeout,
                  Lightbox) {

            const PAGE_SIZE = 4;
            $scope.isLoading = false;

            $scope.loadInstruments = function () {
                $scope.isLoading = true;
                $timeout(function () {
                    usSpinnerService.spin('spinner-1');
                }, 500);

                instrumentsService
                    .getInstruments()
                    .then(function (response) {
                        var results = response.instruments;
                        $scope.instruments = results;
                        var pages = Math.ceil(results.length / PAGE_SIZE);
                        $scope.pages = _.range(pages);
                        $scope.mapValues(results);

                        $scope.isLoading = false;
                        $timeout(function () {
                            usSpinnerService.stop('spinner-1');
                        }, 500);

                        $('#instrumentsCarousel').carousel({
                            interval: 4000,
                            pause: "false"
                        });

                        $('#instrumentsCarousel').on('slid.bs.carousel', function () {
                        });
                    });
            };

            $scope.loadGallery = function () {
                $scope.galleryLoading = true;
                $timeout(function () {
                    usSpinnerService.spin('gallery-spinner');
                }, 500);

                imageService
                    .getGalleryImages()
                    .then(function (response) {
                        var results = response.gallery;
                        $scope.galleryImages = results;
                        var pages = Math.ceil(results.length / 2);
                        $scope.galleryPages = _.range(pages);
                        $scope.galleryValues = $scope.updatedMapValues(results, pages, 2);
                        $scope.galleryLoading = false;

                        $timeout(function () {
                            usSpinnerService.stop('gallery-spinner');
                        }, 500);

                        $('#galleryCarousel').carousel({
                            interval: 4000
                        });

                        $('#galleryCarousel').on('slid.bs.carousel', function () {
                        });
                    });
            };

            $scope.updatedMapValues = function (results, pageLength, pageSize) {
                var ctr = 0;
                var values = [];
                var pages = _.range(pageLength);
                var imagesPerPage = _.range(pageSize);

                _.each(pages, function (i) {
                    values.push({page: i});
                    values[i].items = [];
                    _.each(imagesPerPage, function (value, key) {
                        if (!_.isUndefined(results[ctr])) {
                            values[i].items.push(results[ctr]);
                        }
                        ctr = ctr + 1;
                    });
                });

                return values;
            };

            /*TODO: update to updated function*/
            $scope.mapValues = function (results) {
                var ctr = 0;
                $scope.values = [];
                _.each($scope.pages, function (i) {
                    $scope.values.push({page: i});
                    $scope.values[i].items = [];
                    _.each([1, 2, 3, 4], function (value, key) {
                        if(!_.isUndefined(results[ctr])) {
                            $scope.values[i].items.push(results[ctr]);
                        }
                        ctr = ctr + 1;
                    });
                });
            };

            $scope.pauseSlider = function() {
                $scope.sliderStop = true;
                $('#instrumentsCarousel').carousel('pause');
            };

            $scope.playSlider = function() {
                $scope.sliderStop = false;
                $('#instrumentsCarousel').carousel('cycle');
            };

            $scope.init = function() {
                $scope.loadInstruments();
                $scope.loadGallery();
            };

            $scope.init();

            $scope.openLightboxModal = function (model) {
                var index = _.indexOf($scope.instruments, _.find($scope.instruments, {'model': model}));

                Lightbox.openModal($scope.instruments, index, {
                    templateUrl: "lightbox/custom-template.html"
                });
            };

            $scope.openGalleryLightbox = function (imageUrl) {
                var images = _.clone($scope.galleryImages);
                images.pop();
                var index = _.indexOf(images, _.find(images, {'imageUrl': imageUrl}));

                Lightbox.openModal(images, index, {
                        templateUrl: "lightbox/gallery.html"
                });
            };
        }
    ]);