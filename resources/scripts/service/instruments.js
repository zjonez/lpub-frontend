app
    .service('instrumentsService', ['$q', 'Restangular',
        function ($q, Restangular) {
            var Service = {};
            var data = {};

            data.instruments = [
                {
                    model: 'IDEASCIENCE SLRP001',
                    name: 'ANEMOMETER AND WIND VANE',
                    description: 'These are improvised weather instruments used to measure/describe the speed of the wind and detect its direction. The two instruments are mounted on a single base and stand. The wind cups are made of four teaspoons, one of which has a different color in order to facilitate the counting of the number of revolutions per minute. The wind vane is mounted on a mounting post; the anemometer on a mounting rack. The vane is made of a light rubber sole (or any other appropriate material). Its pointer and pivot are made of bicycle spokes.',
                    size: '6" x 10" x 14"',
                    imageUrl: 'images/uploads/instruments/small/instrument_01.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_01.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP002',
                    name: 'BIMETALLIC THERMOSTAT',
                    description: 'This device is used to demonstrate the function of a bimetallic switch in automating an electric circuit. It consists of a 2.5-V flashlight lamp that is connected in series with a fluorescent tube starter that serves as a bimetallic switch. On standby mode, the circuit is normally open. When the starter is heated, the circuit closes and turns on the lamp. When the starter cools, the circuit shifts back to standby mode.',
                    size: '4" x 7" x 10"',
                    imageUrl: 'images/uploads/instruments/small/instrument_02.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_02.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP003',
                    name: 'BOILING/MELTING POINT APPARATUS',
                    description: 'It is used to measure the boiling point of liquids and the melting point of certain solids as camphor, paraffin, naphthalene, etc. It consists of a boiler, a water bath, and a laboratory thermometer, all of which are mounted on a universal base and stand. Its water bath boiler is an enameled dish that is mounted on a mounting ring. The laboratory thermometer is an alcohol thermometer and has a range of -100C to 1100 C.',
                    size: '6" x 8" x 18"',
                    imageUrl: 'images/uploads/instruments/small/instrument_03.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_03.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP004',
                    name: 'CENTRIFUGAL LOOP',
                    description: 'This apparatus is used to investigate why and how the earth got its shape, ie., an oblate spheroid. It consists of a vinyl strap that is looped into a circular ring, whose ends are screwed vertically to a bicycle spoke , and in such a way that its lower portion is free to slide along the spoke. The bicycle spoke is rotated by a string belt. As the string is pulled and released alternately, the vinyl loop spins and forms a spherical shape. As the loop spins faster, it flattens at its poles.',
                    size: '6" x 8" x 14',
                    imageUrl: 'images/uploads/instruments/small/instrument_04.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_04.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP005',
                    name: 'CHLOROPHYLL EXTRACTION APPARATUS',
                    description: 'This apparatus is actually a water bath boiler that is used to extract chlorophyll from green leaves. The boiler is a test tube immersed in a pool of water in the enameled dish. The test tube contains denatured alcohol as solvent. The solvent is heated indirectly by boiling water in an enameled dish.',
                    size: '6" x 8" x 12',
                    imageUrl: 'images/uploads/instruments/small/instrument_05.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_05.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP006',
                    name: 'CONDUCTION APPARATUS AND CONDUCTOMETER',
                    description: 'This is a two-in-one apparatus that demonstrates thermal conductivity. The conduction apparatus is an aluminum rod that is mounted horizontally and contains a number of small candle rings or hot-melt glue tablets. When heated from one end, the candle rings/tablets start melting and dropping one after another. The conductometer consists of two other metals, iron and copper, each containing a candle ring/tablet that is attached from an equal distance as the first ring/tablet of the conduction rod. When heated, the metal that is most conductive drops its candle ring first.A test tube is mounted on the upper portion of the stand. This is filled with water and heated on the upper portion of the test tube. The water boils on top while it is still cold below. This demonstrates that water is a poor conductor of heat.',
                    size: '6" x 10" x 12',
                    imageUrl: 'images/uploads/instruments/small/instrument_06.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_06.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP007',
                    name: 'CONVECTION TURBINE',
                    description: 'This apparatus is used to show how a convection current can be put to work. It consists of a brass pinwheel that is pivoted at the tip of a safety pin on top of a chimney. A lighted candle or alcohol burner is placed below the chimney. The pinwheel rotates as warm air is pushed up by cold air.',
                    size: '4" x 7" x 12"',
                    imageUrl: 'images/uploads/instruments/small/instrument_07.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_07.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP008',
                    name: 'DEMONSTRATION LEVER',
                    description: 'It is also called a demonstration balance. It is used to investigate the law of levers. It consists of a plastic ruler containing equally spaced holes and mounted at the middle from the shaft of a mounting post. Its adjustment rider is a bulldog clip/laundry clip that can be moved to the left or right of the ruler in order to balance it. A pointer is attached to the ruler. The forces are supplied by paper clips of equal weights.',
                    size: '12" x 7" x 12"',
                    imageUrl: 'images/uploads/instruments/small/instrument_08.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_08.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP009',
                    name: 'DICHROMATE CELL',
                    description: 'This is a model of a voltaic cell which shows how electricity is produced from chemical energy. It can also be used to demonstrate different energy transformations. The electrodes are attached to a mounting plug. The negative electrode is a zinc (G.I.) strip and the positive electrode is a carbon rod. The electrolyte is a solution of sulfuric acid and potassium dichromate. The load is a DVD motor that is connected to the electrodes. The plastic cup is raised to immerse the electrodes in the electrolyte. When not in use, the electrodes are out of the electrolyte, thus preventing the materials from getting used up.',
                    size: '6" x 7" x 14"',
                    imageUrl: 'images/uploads/instruments/small/instrument_09.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_09.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP010',
                    name: 'DIFFERENTIAL RADIOSCOPE',
                    description: 'This radioscope is designed as a differential thermoscope. It consists of a U-tube manometer connected to two air bulbs (test tubes), one of which has a black paper lining, the other a white paper lining and containing a 1-ml disposable syringe. The syringe is used to adjust the manometer liquid to equal levels. The setup is placed under direct sunlight. The two air bulbs will absorb different amounts of heat. The difference in heat absorption is detected by the displacement of the colored water in the U-tube manometer.',
                    size: '6" x 7" x 12"',
                    imageUrl: 'images/uploads/instruments/small/instrument_10.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_10.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP011a',
                    name: 'DIFFERENTIAL THERMOSCOPE (direct versus slanted sunlight)',
                    description: 'A differential thermoscope compares heat absorption between two variables subjected to the same conditions. It consists of a U-tube manometer and two test tubes as air bulbs. One air bulb is connected to one end of the U-tube and is tilted to face the sun directly, and the other to the opposite end of the U-tube and is mounted vertically to receive slanted rays from the sun. Both test tubes have a black paper lining. A 1-mL disposable syringe is attached to one test tube in order to adjust the manometer liquid levels.',
                    size: '6" x 8" x 10"',
                    imageUrl: 'images/uploads/instruments/small/instrument_11.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_11.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP011b',
                    name: 'DIFFERENTIAL THERMOSCOPE (land mass versus body of water)',
                    description: 'This model compares the absorption of heat by land masses and bodies of water. It consists of a U-tube manometer and two test tubes as air bulbs. One air bulb is connected to one end of the U-tube and is immersed in water. The other air bulb is connected to the other end of the U-tube and is embedded in sand. Both test tubes have a black paper lining. A 1-mL disposable syringe is attached to one test tube in order to adjust the manometer liquid levels.',
                    size: '6" x 8" x 10"',
                    imageUrl: 'images/uploads/instruments/small/instrument_12.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_12.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP012',
                    name: 'ELECTRICAL CONDUCTIVITY TESTER/ELECTROLYTIC TESTER',
                    description: 'This apparatus is used to demonstrate the electrical conductivity of materials. It is an open circuit of a battery, an oscillator (toy cell phone) and two stainless steel probes. The probes are joined with a test material. When the circuit is closed, the tester sounds and the test material is a conductor; otherwise, it is an insulator. When used as an electrolytic tester, the probes are immersed in the test liquid. If the oscillator sounds, the liquid is an electrolyte.',
                    size: '4" x 7" x 14"',
                    imageUrl: 'images/uploads/instruments/small/instrument_13.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_13.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP013a',
                    name: 'ELECTRIC CIRCUITS KIT (parallel circuit)',
                    description: 'This parallel circuit kit contains 2 dry cells, 2 lamp holders w/ 2.5-V flashlight bulb each, 1 SPST switch, and 3 connecting wires w/ an alligator clip at each end. The positive terminal of the battery is connected to the V+ terminal screw of the counter trim stand.',
                    size: '4" x 7" x 14"',
                    imageUrl: 'images/uploads/instruments/small/instrument_14.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_14.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP013b',
                    name: 'ELECTRIC CIRCUITS KIT (series circuit)',
                    description: 'This series circuit kit contains 2 dry cells, 2 lamp holders w/ 2.5-V flashlight bulb each, 1 SPST switch, and 3 connecting wires w/ an alligator clip at each end. The V+ terminal screw of the stand is insulated by a plastic tube so that only the red alligator clip is connected to the battery.',
                    size: '4" x 7" x 12"',
                    imageUrl: 'images/uploads/instruments/small/instrument_15.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_15.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP014',
                    name: 'ELECTRIC GENERATOR',
                    description: 'This device is a miniature ceiling fan without its blades that is designed as an electric generator. It is an AC motor whose rotor is a series of permanent magnets. When the rotor is manually rotated, the magnetic field of the permanent magnets cuts through the coil of the electromagnet. This causes a displacement of electrons in the stator coil. The coil is connected to an LED which detects the presence of an electric current. A slight electric shock can also be felt when the coil terminals are held by the hand while the rotor spins.',
                    size: '4" x 7" x 12"',
                    imageUrl: 'images/uploads/instruments/small/instrument_16.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_16.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP015',
                    name: 'ELECTRIC MOTOR',
                    description: 'This is the simplest design of an electric motor. It consists of a coil that is mounted freely on two terminal lugs. One terminal lug is grounded and connected directly to the positive terminal of the battery. The other terminal lug is connected to the base (V-). The coil has 8 turns of magnet wire. Its terminals also serve as the shaft or axle. The insulating lacquer is removed from the lower side of each terminal of the coil in order to make contact with the terminal lugs. To start the motor, the coil is pushed to give it an initial spin.',
                    size: '4" x 7" x 12"',
                    imageUrl: 'images/uploads/instruments/small/instrument_17.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_17.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP016',
                    name: 'ELECTROLYTIC CELL (electrolysis apparatus)',
                    description: 'Otherwise known as electrolysis apparatus, this device demonstrates the electrolysis of water. It consists of two stainless steel stove bolts as electrodes. The anode has a red lead wire connected to the stand and to the positive terminal of the battery, while the cathode has a black lead wire connected to the base and to the negative terminal. The electrodes are attached to the plastic cup or tray containing a dilute solution of sulfuric acid or sodium hydroxide. A test tube filled with water and sulfuric acid is inverted into the electrolyte and suspended over each electrode. Bubbles are formed when electricity passes through the electrolyte.',
                    size: '6" x 7" x 12"',
                    imageUrl: 'images/uploads/instruments/small/instrument_18.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_18.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP017',
                    name: 'ELECTROMAGNETIC CRANE',
                    description: 'This apparatus consists of an electromagnet, a crane, a windlass, and V+ terminal screw that serves also as a switch. The electromagnet is suspended from the upper end of a glass clip which serves as the crane. This end of the crane is tied to the axle of the windlass with a cotton string. When the handle of the windlass is turned, the crane raises or lowers the electromagnet. The electromagnet can be turned on and off by connecting or disconnecting the V+ terminal screw to the positive terminal of the battery.',
                    size: '6" x 10" x 12"',
                    imageUrl: 'images/uploads/instruments/small/instrument_19.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_19.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP018',
                    name: 'ELECTROPLATING APPARATUS (copper plating)',
                    description: 'This apparatus is used to demonstrate the principles of electroplating, particularly copper plating. The anode is a copper strip while the cathode is the metal to be plated (e.g. coin). The cathode is suspended from an alligator clip. Both electrodes are immersed in a copper sulfate solution treated with a little sulfuric acid to increase its conductivity. The electrodes are connected in series with a 2.5-V lamp acting as a limiting resistor.',
                    size: '6" x 8" x 8"',
                    imageUrl: 'images/uploads/instruments/small/instrument_20.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_20.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP019',
                    name: 'EQUAL ARM BEAM BALANCE',
                    description: 'This is an improvised equal arm beam balance. It consists of a plastic ruler that is mounted from the shaft of a mounting post. A weighing pan is suspended from each end of the ruler. A laundry clip is used as a rider to balance the beam. The sample object to be weighed is placed on the left-hand pan. The counter-balancing mass is placed at the right-hand pan. In the absence of a set of standard mass, water may be used to counter-balance the sample object. One mL of water is equal to 1 gram.',
                    size: '12" x 7" x 12"',
                    imageUrl: 'images/uploads/instruments/small/instrument_21.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_21.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP020',
                    name: 'EXPANSION OF AIR APPARATUS',
                    description: 'This apparatus is used to demonstrate the cubical expansion/ contraction of air when heated/cooled. It consists of a test tube containing a rubber balloon. When the test tube is heated, the air inside expands and the balloon gets partially inflated. When cooled, the air contracts and balloon gets deflated.',
                    size: '6" x 8" x 8"',
                    imageUrl: 'images/uploads/instruments/small/instrument_22.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_22.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP021',
                    name: 'FILTRATION AND EVAPORATION APPARATUS',
                    description: 'This is a two-in-one apparatus. The filtration apparatus consists of a plastic funnel that is mounted on the upper end of the stand. A filter paper is folded into the shape of the funnel and placed inside the funnel. The evaporation apparatus consists of an enameled dish that is mounted onto a mounting ring at the lower end of the stand. It also serves as a collecting vessel for the filtrate. If the filtrate is a solution, the solute can be retrieved by evaporating the solvent.',
                    size: '6" x 7" x 10"',
                    imageUrl: 'images/uploads/instruments/small/instrument_23.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_23.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP022',
                    name: 'FIRE ALARM',
                    description: 'Fire Alarm is used to demonstrate how a bimetal can be used to make or break an alarm circuit by means of heat. It consists of an oscillator alarm (toy cell phone) that is connected to a fluorescent tube starter. When the starter is heated, its bimetallic strip bends and makes contact with its terminal pin. The circuit closes and the alarm is triggered. When the starter cools, the circuit opens and disables the alarm.',
                    size: '6" x 7" x 14"',
                    imageUrl: 'images/uploads/instruments/small/instrument_24.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_24.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP023',
                    name: 'GEYSER (demonstration model)',
                    description: 'This is a working model of a geyser. It is used to demonstrate that the earth has geothermal energy. It consists of an 18 mm x 150 mm test tube, a # 1 rubber stopper, a g20 x 1-1/2 disposable injection needle, a glass tubing, a plastic catch basin, and an alcohol burner that are all mounted on a universal base and stand. The injection needle passes through a hole into the mounting rack and through a catch basin. When the water is heated, pressure is developed inside the test tube. The increased pressure pushes the water through the glass tubing and leaves the injection needle as a jet of hot water and steam.',
                    size: '4" x 7" x 14"',
                    imageUrl: 'images/uploads/instruments/small/instrument_25.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_25.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP024',
                    name: 'HOT-COLD AIR BALANCE',
                    description: 'This apparatus is simply an equal arm beam balance. A paper bag is suspended from each end of the beam. A bulldog clip is used as a rider to balance the beam when both paper bags are inflated. To show that hot air is lighter than cold air, the air in one bag is heated with a lighted candle.',
                    size: '14" x 8" x 16"',
                    imageUrl: 'images/uploads/instruments/small/instrument_26.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_26.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP025',
                    name: 'INERTIA APPARATUS',
                    description: 'This apparatus demonstrates the first part of Newton\'s first law of motion, i.e., a body at rest tends to remain at rest. It consists of a striker made of a flexible plastic teaspoon, a 2" x 2" vinyl plate or cardboard, and a glass marble. The plate is balanced on the vertical stand in such a way that one edge is touching the striker perpendicularly. The marble, or any object, is placed on top of the plate directly above the stand.',
                    size: '6" x 8" x 8"',
                    imageUrl: 'images/uploads/instruments/small/instrument_27.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_27.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP026',
                    name: 'LEAD-ACID STORAGE CELL',
                    description: 'A lead-acid storage cell is a secondary cell. It differs from a primary cell in that it can be recharged once its electrical energy is used up. It consists of two lead electrodes that are suspended from a mounting plug. The electrodes are immersed in a pool of sulfuric acid. When the electrodes are connected to the battery, electrolysis converts electrical energy to chemical energy. This process charges the cell. After the cell is charged for a few minutes, the connecting wires are transferred to the DVD motor. The motor turns to show that the cell delivers electric current.',
                    size: '6" x 8" x 16"',
                    imageUrl: 'images/uploads/instruments/small/instrument_28.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_28.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP027',
                    name: 'LINEAR EXPANSION APPARATUS',
                    description: 'This apparatus is used to demonstrate the linear expansion of a metal rod. The rod is mounted to a glass clip on an angular brace. A pointer is inserted between the rod and the angular brace. They are held together by a rubber band. When the rod is heated, it expands. The expansion is detected by the pointer.',
                    size: '12" x 7" x 12"',
                    imageUrl: 'images/uploads/instruments/small/instrument_29.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_29.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP028',
                    name: 'LOOP-THE-LOOP',
                    description: 'This apparatus is used to demonstrate conservation of potential energy to kinetic energy and vice versa. It is so called because its shape is like that of an airplane that loops the loop. It is made of an aluminum I-beam is bent into a single loop with one end very much longer than its other end. It is mounted vertically so that its longer end is raised like an inclined plane. When a glass marble is released from its upper end, it rolls down the inclined track and loops the loop.',
                    size: '4" x 12" x 18"',
                    imageUrl: 'images/uploads/instruments/small/instrument_30.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_30.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP029',
                    name: 'MASON’S HYGROSCOPE',
                    description: 'This instrument is also known as the Sling\'s Psychrometer or the wet- bulb & dry-bulb hygrometer. It is used to detect changes in relative humidity. It consists of a U-tube manometer, two test tubes, delivery tubes, and a disposable syringe, all of which are mounted on an aluminum base and stand. A wad of moist cotton is wrap over the end of one air bulb. The difference in the height of the water columns inside the U-tube determines the relative humidity at a given time.',
                    size: '6" x 7" x 14"',
                    imageUrl: 'images/uploads/instruments/small/instrument_31.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_31.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP030',
                    name: 'MASS DENSITY APPARATUS',
                    description: 'This apparatus is used to measure the density of a liquid. The sample liquid is placed in the left-hand syringe. Water is placed into the right-hand syringe in order to balance the beam. Since the volume of water in milliliters is equal to its mass in grams, the mass of the liquid is equal to the mass of the water. The volume of the liquid is measured by the syringe.',
                    size: '6" x 8" x 8"',
                    imageUrl: 'images/uploads/instruments/small/instrument_32.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_32.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP031',
                    name: 'NEUROMUSCULAR ACUITY TESTER',
                    description: 'This device can be used to test one\'s dexterity. It can also demonstrate how steady is one\'s nerves under pressure or excitement. It is basically a device that can detect how well coordinated are the nerves and the muscles. It consists of an electronic oscillator (toy cell phone) connected in series with a wire maze and a looped probe. The maze and the probe provide the switching mechanism of the device.',
                    size: '4" x 10" x 14"',
                    imageUrl: 'images/uploads/instruments/small/instrument_33.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_33.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP032',
                    name: 'PHASE CHANGE APPARATUS',
                    description: 'It is used to demonstrate the different phase changes that water undergoes at different temperatures. It consists of a boiler, which is a 16 mm x 150 mm test tube, and a condenser, which is an enameled dish filled with ice. When water evaporates from the boiler, the steam reaches the condenser and is turned back to liquid.',
                    size: '6" x 8" x 16"',
                    imageUrl: 'images/uploads/instruments/small/instrument_34.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_34.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP033',
                    name: 'RAIN GAUGE',
                    description: 'It consists of a plastic funnel and a test tube that are mounted on a universal base and stand. The funnel serves as the catch basin while the test tube serves as the collecting vessel for the rain. The instrument is used to measure rainfall.',
                    size: '4" x 7" x 10"',
                    imageUrl: 'images/uploads/instruments/small/instrument_35.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_35.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP034',
                    name: 'SIMPLE DISTILLATION APPARATUS',
                    description: 'The apparatus is used to distill water and study its phase changes under different temperatures. It consists of an 18 mm x 150 mm test tube boiler, a #1 rubber stopper and delivery tube, and a condenser test tube immersed in a plastic cup of cold water. When the boiler is heated, water turns to steam. The steam passes through the delivery tube into the condenser test tube, where it turns back to liquid.',
                    size: '8" x 7" x 12"',
                    imageUrl: 'images/uploads/instruments/small/instrument_36.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_36.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP035',
                    name: 'STEAM TURBINE',
                    description: 'This improvised apparatus is used to demonstrate the transformation of thermal energy to mechanical energy. It consists of a test tube boiler, an injection needle and a brass turbine. The boiler is mounted on the base. The injection needle passes through a mounting rack. The brass pinwheel is pivoted on top of a safety pin on top of the mounting rack. When the boiler is heated, steam is formed as a jet from the needle against the pinwheel. As the steam leaves the pinwheel, its action produces a spinning reaction of the pinwheel.',
                    size: '4" x 7" x 14"',
                    imageUrl: 'images/uploads/instruments/small/instrument_37.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_37.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP036',
                    name: 'STRENGTH OF LEG BONE APPARATUS',
                    description: 'This device simulates the strength of a leg bone. It consists of a horizontal beam whose one end is mounted to the stand in such a way that its other end can freely turn. A plastic tumbler is mounted to the beam at the outer end. A flattened plastic drinking straw and a hollow drinking straw are used one at a time to support the beam and the tumbler of sand. You can compare the strength of straws in their ability to support the tumbler of sand.',
                    size: '4" x 12" x 10"',
                    imageUrl: 'images/uploads/instruments/small/instrument_38.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_38.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP037',
                    name: 'TELLURIAN',
                    description: 'This is a model of the solar system consisting of the sun, the moon, and the earth. It is used to demonstrate the simultaneous rotations and revolutions of the earth and the moon, and the concepts of night and day, eclipses, phases of the moon, and divisions of time. The model is constructed from an anodized aluminum base with an I-beam radial arm, two pulleys, and a crossbelt drive. The two pulleys are in a ratio of 1:12 in order to simulate the 12 months in one year. The sun is represented by a 5" yellow plastic ball, and its light rays are supplied by a mini-flashlight. The earth is represented by a miniature globe mounted on a tilted axis. The moon is represented by a golden bead fixed to an axle. Half of the bead is painted black; this side is known as the "dark side" of the moon. ',
                    size: '6" x 8" x 20"',
                    imageUrl: 'images/uploads/instruments/small/instrument_39.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_39.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP038',
                    name: 'U-TUBE AIR THERMOSCOPE',
                    description: 'This apparatus is used to demonstrate the working principle of thermometers; vis-a-vis expansion and contraction of matter when heated and cooled, respectively. It consists of a black test tube that is connected to a U-tube manometer. The test tube serves as an air bulb that senses the changes in air temperature. The displacement of the colored water in the U-tube detects the changes in the temperature of the air bulb. A 1-mL disposable syringe connected to the rubber stopper of the test tube serves to adjust the height of the water columns inside the U-tube.',
                    size: '6" x 7" x 14"',
                    imageUrl: 'images/uploads/instruments/small/instrument_40.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_40.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP039',
                    name: 'WATER & ALCOHOL THERMOSCOPE',
                    description: 'This device demonstrates the working principle of thermometers, vis-à-vis the expansion and contraction of the liquid in the bulb. In this device, the bulb is a test tube filled completely with a mixture of colored water and alcohol. A rubber stopper containing a glass tubing is plugged into the test tube. When the bulb is heated, the liquid expands. The small amount of expansion is magnified by the thin glass tubing.',
                    size: '4" x 7" x 16"',
                    imageUrl: 'images/uploads/instruments/small/instrument_41.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_41.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP040',
                    name: 'WATER TURBINE',
                    description: 'It consists of a water wheel that is mounted freely under a water tank. The water wheel is made from four teaspoons that are mounted onto a rubber hub. The teaspoons serve as paddles or blades of the wheel. The water tank is made from two plastic cups with a plastic straw spout. The materials are mounted on a universal base and stand.',
                    size: '4" x 12" x 16"',
                    imageUrl: 'images/uploads/instruments/small/instrument_42.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_42.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP001',
                    name: 'ANEMOMETER AND WIND VANE',
                    description: 'These are improvised weather instruments used to measure/describe the speed of the wind and detect its direction. The two instruments are mounted on a single base and stand. The wind cups are made of four teaspoons, one of which has a different color in order to facilitate the counting of the number of revolutions per minute. The wind vane is mounted on a mounting post; the anemometer on a mounting rack. The vane is made of a light rubber sole (or any other appropriate material). Its pointer and pivot are made of bicycle spokes.',
                    size: '6" x 10" x 14"',
                    imageUrl: 'images/uploads/instruments/small/instrument_01.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_01.jpg'
                },
                {
                    model: 'IDEASCIENCE SLRP002',
                    name: 'BIMETALLIC THERMOSTAT',
                    description: 'This device is used to demonstrate the function of a bimetallic switch in automating an electric circuit. It consists of a 2.5-V flashlight lamp that is connected in series with a fluorescent tube starter that serves as a bimetallic switch. On standby mode, the circuit is normally open. When the starter is heated, the circuit closes and turns on the lamp. When the starter cools, the circuit shifts back to standby mode.',
                    size: '4" x 7" x 10"',
                    imageUrl: 'images/uploads/instruments/small/instrument_02.jpg',
                    imageLarge: 'images/uploads/instruments/large/instruments-large_02.jpg'
                }
            ];

            Service.getInstruments = function () {
                var deferred = $q.defer();
                deferred.resolve(data);
                return deferred.promise;
            };

            return Service;
        }
    ]);