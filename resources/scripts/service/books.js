app
    .service('bookService', ['$q', 'Restangular',
        function ($q, Restangular) {
            var Service = {};
            var data = {};

            Service.getSeries = function() {
                var deferred = $q.defer();

                deferred.resolve(
                    Restangular
                        .all('series')
                        .customGET("")
                );

                return deferred.promise;
            };

            Service.getBooks = function() {
                var deferred = $q.defer();

                deferred.resolve(
                    Restangular
                        .all('books')
                        .customGET("")
                );

                return deferred.promise;
            };

            return Service;
        }
    ]);