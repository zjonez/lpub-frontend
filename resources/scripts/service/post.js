app
    .service('postService', ['$q', 'Restangular',
        function ($q, Restangular) {
            var Service = {};

            Service.get = function (page) {
                var deferred = $q.defer();
                var params = {page: page};

                deferred.resolve(
                    Restangular
                        .all('get-posts')
                        .customGET("", params)
                );

                return deferred.promise;
            };


            return Service;
        }
    ]);