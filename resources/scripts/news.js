app
    .controller('newsController', [
        'SETTINGS',
        '$scope',
        'usSpinnerService',
        'postService',
        '$timeout',
        function (
                SETTINGS,
                $scope,
                usSpinnerService,
                postService,
                $timeout
        ) {

                var PAGE_SIZE = 3;
                $scope.settings = JSON.parse(SETTINGS);
                $scope.filters = {};

                $scope.isLoading = false;

                $scope.getPosts = function () {
                        $scope.isLoading = true;
                        $timeout(function () {
                                usSpinnerService.spin('spinner-1');
                        }, 500);

                        postService
                            .get($scope.settings.page)
                            .then(function (response) {
                                    $scope.posts = response.posts;
                                    var pages = Math.ceil(response.count / PAGE_SIZE);
                                    $scope.pages = _.range(pages);
                                    $scope.isLoading = false;
                                    $timeout(function () {
                                            usSpinnerService.stop('spinner-1');
                                    }, 500);
                            });
                };

                $scope.init = function() {
                        $scope.getPosts();
                };

                $scope.init();
        }
    ]);