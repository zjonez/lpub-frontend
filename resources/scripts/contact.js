app
    .controller('contactController', [
        '$scope',
        'usSpinnerService',
        '$timeout',
        '$q',
        'Restangular',
        function ($scope,
                  usSpinnerService,
                  $timeout,
                  $q,
                  Restangular) {
            $scope.params = {};
            $scope.isLoading = false;

            $scope.submitForm = function () {
                if ($scope.contactForm.$valid) {
                    $scope.isLoading = true;
                    $timeout(function () {
                        usSpinnerService.spin('spinner-1');
                    }, 500);

                    //@TODO: move this to service

                    Restangular
                        .all('contact-us')
                        .post($scope.params)
                        .then(function (response) {
                            $scope.isLoading = false;

                            $timeout(function () {
                                usSpinnerService.stop('spinner-1');
                            }, 500);

                            $scope.status = response.success;
                            $scope.message = response.message;
                        });
                }
            };
        }
    ]);