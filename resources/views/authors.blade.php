@extends('layouts.app')

@section('header')
    <link href="css/about.css" rel="stylesheet" type='text/css'/>
@endsection

@section('content')
    <div class="row-fluid" data-spy="scroll" data-target=".container" data-offset="50">
        <div class="container">
            <div class="row-fluid caption">
                <h2 class="primary">AUTHORS</h2>
                <h1 class="secondary">ABOUT INSTRUCTORS</h1>
            </div>

            <hr>

            <div class="row-fluid detailed-authors">
                @foreach($authors as $data)
                    <div class="row detailed-author" id="{{Str::lower($data->lastName)}}">
                        <div class="col-md-2">
                            <div class="row-fluid image">
                                <img ng-src="{{$data->imageUrl}}" alt="Image"
                                     class="img-responsive" />
                            </div>
                        </div>
                        <div class="col-md-10">
                            <p class="pull-left text-blue">
                                <strong>
                                    {{Str::upper($data->firstName)}}
                                    {{Str::upper($data->middleName)}}.
                                    {{Str::upper($data->lastName)}}
                                </strong>
                            </p>

                            <div class="clearfix"></div>

                            <div class="row-fluid full-description">
                                <p>
                                    {!!$data->fullDescription!!}
                                </p>
                            </div>
                        </div>
                    </div>

                    <hr>
                @endforeach
            </div>
        </div>
    </div>
@endsection