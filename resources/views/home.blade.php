@extends('layouts.app')

@section('header')
    <link href="css/home.css" rel="stylesheet" type='text/css'>
    <script src="js/service/images.js"></script>
    <script src="js/service/books.js"></script>
    <script src="js/home.js"></script>
@endsection

@section('content')
    <div class="row-fluid" ng-controller="homeController">
        <div class="row home-slider">
            <div id="homeCarousel" class="carousel slide" data-ride="carousel" ng-show="!isLoading">
                <div class="carousel-inner">
                    <div ng-repeat="image in images | orderBy: 'sequence'" class="item" ng-class="{active : $first}">
                        <div class="row slider-images">
                            <img ng-src="<%image.imageUrl%>" alt="Image"
                                 class="img-responsive" />
                            <div class="slider-caption" ng-if="image.caption">
                                <p class="desc" ng-bind-html="image.caption | sanitize"></p>
                            </div>
                        </div>
                    </div>
                </div>

                <a class="left carousel-control" href="#homeCarousel" data-slide="prev">
                    <i class="fa fa-chevron-left" aria-hidden="true"></i>
                </a>
                <a class="right carousel-control" href="#homeCarousel" data-slide="next">
                    <i class="fa fa-chevron-right" aria-hidden="true"></i>
                </a>
            </div>
            <div us-spinner="{radius:30, width:8, length: 16}" class="lpub-spinner" spinner-key="spinner-1"></div>
        </div>

        <div class="container products">
            <div class="row-fluid caption">
                {{--<p class="primary">LEARNED PUBLISHING</p>--}}
                {{--<p class="secondary">PRODUCTS</p>--}}
                <h2 class="primary">LEARNED PUBLISHING</h2>
                <h1 class="secondary">PRODUCTS</h1>
            </div>
            <div class="row-fluid">
                <div id="productsCarousel" class="carousel slide" data-ride="carousel" ng-show="!productsLoading">
                    <div class="row">
                        <div class="pull-right">
                            <ol class="carousel-indicators">
                                <li ng-repeat="page in bookPages" data-target="#productsCarousel" data-slide-to="<%page%>" ng-class="{active : $first}"></li>
                                <li ng-show="!sliderStop" class="slider-options">
                                    <a ng-class="{'active-slider': !sliderStop}" ng-click="pauseSlider()">
                                        <span class="fa fa-pause"></span>
                                    </a>
                                </li>
                                <li ng-show="sliderStop" class="slider-options">
                                    <a ng-class="{'active-slider': sliderStop}" ng-click="playSlider()">
                                        <span class="fa fa-play"></span>
                                    </a>
                                </li>
                            </ol>
                        </div>
                    </div>
                    <div class="carousel-inner">
                        <div ng-repeat="books in bookValues" class="item" ng-class="{active : $first}">
                            @include('layouts.product-slider')
                        </div>
                    </div>
                </div>
                <div us-spinner="{radius:30, width:8, length: 16}" class="lpub-spinner" spinner-key="spinner-2"></div>
            </div>
        </div>

        <div class="container">
            <div class="row page-nav text-center">
                <div class="col-md-4 about-us">
                    <div class="row nav-item">
                        <a href="about-us" class="hvr-reveal"><strong>ABOUT US</strong></a>
                    </div>
                </div>
                <div class="col-md-4 services">
                    <div class="row nav-item">
                        <a href="services" class="hvr-reveal"><strong>SERVICES</strong></a>
                    </div>
                </div>
                <div class="col-md-4 contact-us">
                    <div class="row nav-item">
                        <a href="contact-us" class="hvr-reveal"><strong>CONTACT US</strong></a>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection