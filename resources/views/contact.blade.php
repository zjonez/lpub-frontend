@extends('layouts.app')

@section('header')
    <link href="css/contact.css" rel="stylesheet" type='text/css'/>
    <script src="js/contact.js"></script>
@endsection

@section('content')
    <div class="row-fluid" ng-controller="contactController">
        <div class="container contact-us">
            <div class="row-fluid caption">
                <h1 class="secondary">CONTACT US</h1>
            </div>

            <div class="row-fluid">
                <iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d1930.2943448129!2d121.12674134074815!3d14.622491357356509!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x0%3A0x0!2zMTTCsDM3JzIwLjkiTiAxMjHCsDA3JzQwLjIiRQ!5e0!3m2!1sen!2sph!4v1466736426181"
                        width="100%" height="450" frameborder="0" style="border:0" allowfullscreen></iframe>
            </div>

            <div class="row-fluid main-content">
                <div class="col-md-6">
                    <div class="row">
                        <div class="alert" ng-class="{'alert-danger': !status, 'alert-success': status}"
                             ng-show="message && !isLoading">
                            <strong ng-show="!status">Error!</strong><strong ng-show="status">Success!</strong>
                            <%message%>
                        </div>
                        <form name="contactForm" ng-submit="submitForm()" ng-show="!isLoading" novalidate>
                            <div class="form-group"
                                 ng-class="{ 'has-error' : contactForm.name.$invalid && !contactForm.name.$pristine }">
                                <label for="name"><strong>Name</strong></label>
                                <input id="name" type="text" class="form-control" ng-model="params.name"
                                       ng-required="true">
                                <p ng-show="contactForm.name.$invalid && !contactForm.name.$pristine"
                                   class="help-block">You name is required.</p>
                            </div>

                            <div class="form-group"
                                 ng-class="{ 'has-error' : contactForm.email.$invalid && !contactForm.email.$pristine }">
                                <label for="email"><strong>Email</strong></label>
                                <input id="email" type="email" class="form-control" ng-model="params.email"
                                       ng-required="true">
                                <p ng-show="contactForm.email.$invalid && !contactForm.email.$pristine"
                                   class="help-block">Enter a valid email.</p>
                            </div>

                            <div class="form-group"
                                 ng-class="{ 'has-error' : contactForm.subject.$invalid && !contactForm.subject.$pristine }">
                                <label for="subject"><strong>Subject</strong></label>
                                <input id="subject" type="text" class="form-control" ng-model="params.subject"
                                       ng-required="true">
                                <p ng-show="contactForm.subject.$invalid && !contactForm.subject.$pristine"
                                   class="help-block">subject is required.</p>
                            </div>

                            <div class="form-group"
                                 ng-class="{ 'has-error' : contactForm.message.$invalid && !contactForm.message.$pristine }">
                                <label for="message"><strong>Message</strong></label>
                                <textarea id="message" class="form-control" rows="3" ng-model="params.message"
                                          ng-required="true"></textarea>
                                <p ng-show="contactForm.message.$invalid && !contactForm.message.$pristine"
                                   class="help-block">message is required.</p>
                            </div>

                            <button type="submit" class="btn btn-orange" ng-disabled="contactForm.$invalid">Submit
                            </button>
                        </form>
                        <div us-spinner="{radius:30, width:8, length: 16}" class="lpub-spinner"
                             spinner-key="spinner-1"></div>
                    </div>
                </div>
                <div class="col-md-5 col-md-offset-1">
                    <div class="row">
                        <p>Kindly use the contact form to the left, if you would like to inquire to us through
                            email.</p>
                        <p>You may also contact us through the following:</p>

                        <p><strong>Hotline:</strong></p>
                        <ul>
                            <li>647-9410</li>
                            <li>647-4374 (telefax)</li>
                        </ul>

                        <p><strong>Address:</strong></p>
                        <ul>
                            <li>No. 64 Camia corner, Sampaguita St. Lourdes Subd. Brgy. Mambugan Antipolo City 1870</li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection
