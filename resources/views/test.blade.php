@extends('layouts.app')
@section('header')
    <link href="css/products.css" rel="stylesheet" type='text/css'/>
@endsection

@section('content')
    <p>
        Lorem ipsum dolor sit amet, consectetur adipiscing elit. Mauris tortor arcu, congue
        consequat accumsan eget, aliquam et augue. Quisque vitae tincidunt leo. Fusce et urna
        placerat, semper lectus nec, dapibus elit. Praesent volutpat velit eu dui interdum
        ultricies. Sed rhoncus augue sed est accumsan, sit amet accumsan urna porttitor. Duis
        consectetur bibendum nisl, non blandit orci rutrum facilisis. Nullam ac turpis a nisi
        scelerisque consequat in eu enim. Aliquam vulputate leo id felis bibendum volutpat. Cum
        sociis natoque penatibus et magnis dis parturient montes, nascetur ridiculus mus. Morbi
        tempor leo ipsum, sit amet iaculis augue porttitor sed. Curabitur rhoncus accumsan
        commodo.
    </p>
    <p>
        Praesent fermentum erat vitae ex iaculis blandit quis id eros. Vivamus molestie nunc a
        arcu sollicitudin dictum. Vestibulum tempor pellentesque quam, nec fermentum neque
        mattis a. Vivamus viverra, risus non eleifend tincidunt, est magna rutrum erat, eu
        maximus ligula dolor eu nisi. Integer tempus libero sit amet magna consectetur
        malesuada. Nunc interdum mi ut enim elementum euismod. Etiam convallis, turpis sed porta
        rutrum, nisl tortor dignissim nulla, a elementum dui sem feugiat lectus. Nullam egestas
        lectus vitae velit consectetur, a dictum orci eleifend. In eleifend metus purus, in
        vestibulum libero placerat non. Proin est neque, sodales ut lacus at, pellentesque
        vestibulum arcu. Donec at massa eu ante semper ultricies.
    </p>
    <p>
        Sed dignissim massa quis blandit imperdiet. Maecenas purus ligula, malesuada ut lorem
        nec, ultrices posuere lorem. Etiam commodo nibh eget dolor iaculis consectetur. Aenean
        libero elit, ullamcorper sit amet varius vitae, commodo non odio. Proin pharetra massa
        sed accumsan cursus. Sed aliquam enim eget lectus scelerisque, vel placerat lectus
        molestie. Interdum et malesuada fames ac ante ipsum primis in faucibus. Vestibulum
        tempus imperdiet gravida. In lobortis hendrerit mi quis mollis. Etiam eu velit eu erat
        laoreet pharetra. Pellentesque ut viverra elit. Ut vehicula est in arcu pharetra, ut
        aliquam ex varius.
    </p>
@endsection

