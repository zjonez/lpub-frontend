@extends('layouts.app')

@section('header')
    <link href="css/products.css" rel="stylesheet" type='text/css'/>
    <script src="js/service/books.js"></script>
    <script src="js/products.js"></script>
@endsection

@section('content')
    <div class="row-fluid products-lpub" ng-controller="productsController">
        <div class="container">
            <div class="row-fluid caption">
                <h2 class="primary">PRODUCTS</h2>
                <h1 class="secondary">BOOKS OF LEARNED PUBLISHING</h1>
            </div>

            <!--loop through series and books-->
            <div ng-repeat="data in series track by $index" class="row series" ng-show="!isLoading" ng-init="parentIndex = $index">
                <div class="col-md-4">
                    <div class="row frame">
                        <img ng-src="<%data.imageUrl%>" alt="Image" class="img-responsive" />
                    </div>
                </div>
                <div class="col-md-8">
                    <div class="row desc">
                        <p ng-bind-html="data.description | sanitize"></p>

                        <p ng-show="data.hasContentDiagram == 1">
                            <a href='/images/content-diagram.png' target="_blank" class="btn btn-default btn-orange">
                                View Content Diagram
                            </a>
                        </p>
                    </div>
                </div>

                <div class="col-md-12">
                    <div class="row products">
                        <div id="productsCarousel_<%$index%>" class="carousel slide">
                            <div class="row">
                                <div class="pull-right">
                                    <ol class="carousel-indicators">
                                        <li ng-repeat="page in data.bookPages" data-target="#productsCarousel_<%parentIndex%>" data-slide-to="<%page%>"
                                            ng-class="{active : $first}"></li>
                                        <li ng-show="!data.sliderStop" class="slider-options">
                                            <a ng-class="{'active-slider': !data.sliderStop}" ng-click="pauseSlider(data, parentIndex)">
                                                <span class="fa fa-pause"></span>
                                            </a>
                                        </li>
                                        <li ng-show="data.sliderStop" class="slider-options">
                                            <a ng-class="{'active-slider': data.sliderStop}" ng-click="playSlider(data, parentIndex)">
                                                <span class="fa fa-play"></span>
                                            </a>
                                        </li>
                                    </ol>
                                </div>
                            </div>
                            <div class="carousel-inner">
                                <div ng-repeat="books in data.bookValues" class="item" ng-class="{active : $first}">
                                    @include('layouts.product-slider')
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div us-spinner="{radius:30, width:8, length: 16}" class="lpub-spinner" spinner-key="spinner-1"></div>
        </div>

        @include('layouts.content-diagram')
    </div>
@endsection