<!DOCTYPE html>
<html lang="en">
	<head>
		<meta charset="utf-8">
		<meta name="viewport" content="width=device-width, initial-scale=1">

		{!! SEOMeta::generate() !!}
		{!! OpenGraph::generate() !!}

	    <link href='https://fonts.googleapis.com/css?family=Source+Sans+Pro' rel='stylesheet' type='text/css' />

	    <link href="/css/vendor/bootstrap.min.css" rel="stylesheet" type='text/css' />
	    <link href="/css/vendor/font-awesome-4.6.3/css/font-awesome.min.css" rel="stylesheet" type='text/css' />
		<link href="/css/vendor/hover-min.css" rel="stylesheet" type="text/css" />
		<link href="/css/vendor/angular-bootstrap-lightbox.min.css" rel="stylesheet" type="text/css" />
	    <link href="/css/styles.css" rel="stylesheet" type='text/css'>

		<script src="/js/vendor/jquery.min.js"></script>
		<script src="/js/vendor/bootstrap.min.js"></script>
		<script src="/js/vendor/angular.min.js"></script>
		<script src="/js/vendor/spin.js"></script>
		<script src="/js/vendor/angular-spinner.min.js"></script>
		<script src="/js/vendor/lodash.js"></script>
		<script src="/js/vendor/restangular.min.js"></script>
		<script src="/js/vendor/angular-animate.min.js"></script>
		<script src="/js/vendor/ui-bootstrap-tpls.min.js"></script>
		<!-- <script src="js/vendor/angular-bootstrap-lightbox.min.js"></script> -->

		<!-- FF lightbox fix -->
		<script src="/js/vendor/angular-bootstrap-lightbox-tender.min.js"></script>

		<script type="text/javascript">
			var SETTINGS = '{}';
			@if(isset($settings))
				var SETTINGS = '{!! $settings !!}';
			@endif
		</script>

		<!-- main app -->
		<script src="/js/app.js"></script>

		<!-- page specific assets -->
		@yield('header')
	</head>
	<body ng-app="app">
		<div class="lpub-container">
		    <div class="row header">
		        <div class="col-sm-12 col-sm-offset-1">
		            <div class="col-sm-1 text-center">
						<a href="/">
							<img src="/images/lp-logo.jpg" alt="lp-logo">
						</a>
		            </div>
		            <div class="col-sm-11 right-header">

						<!--@TODO: quotes should be DB Driven-->
						<div class="row-fluid">
							<div ng-controller="mainController" class="col-sm-5 quotes">
								<p class="text-grey" ng-bind="quote.text"></p>
								<p class="text-blue pull-right"> - <%quote.origin%> </p>
							</div>
							<div class="col-sm-5 lpub-info">
								<div class="row">
									<span class="col-xs-1"><i class="fa fa-map-marker icon-orange"></i></span>
									<span class="col-xs-10">
		                        	No. 64 Camia corner, Sampaguita St. Lourdes Subd. Brgy. Mambugan Antipolo City 1870
		                        </span>
								</div>
								<div class="row">
									<span class="col-xs-1"><i class="fa fa-phone icon-orange"></i></span>
									<span class="col-xs-10">647-9410 / 647-4374 (telefax)</span>
								</div>
								<div class="row">
									<span class="col-xs-1"><i class="fa fa-envelope-o icon-orange"></i></span>
									<span class="col-xs-10">inquire@learnedpublishing.com</span>
								</div>
							</div>
						</div>
		            </div>
		        </div>
		    </div>
		    
		    <!-- navigation -->
		    <div class= "row-fluid text-center">
		    	<div class="lpub-nav header-nav hidden-xs">
			    	<ul class="nav nav-pills">
	            		@include('layouts.navigation')
	            	</ul>
	    		</div>

				<!-- collapsed nav -->
				<nav class="lpub-nav-collapsed navbar navbar-default hidden-sm hidden-md hidden-lg" role="navigation">
					<div class="navbar-header">
						<button type="button" class="navbar-toggle" data-toggle="collapse"
								data-target="#lpub-navbar-collapse">
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
							<span class="icon-bar"></span>
						</button>
					</div>
					<div class="collapse navbar-collapse" id="lpub-navbar-collapse">
						<ul class="nav navbar-nav">
							@include('layouts.navigation')
						</ul>
					</div>
				</nav>
		    </div>

            <!-- main content -->
			@yield('content')

	    	<!-- footer -->
			<div class="row">&nbsp;</div><!--@TODO: research better way to add space-->

			<div class="row-fluid">
				<footer>
					<!-- bottom nav -->
					<div class="row-fluid footer-nav">
				        <div class="lpub-nav bottom-nav hidden-xs">
					        <ul class="nav nav-pills">
				            	@include('layouts.navigation')
		                        <li class="pull-right top-button">
		                            <a>
		                                TOP
		                                <img src="/images/Nav_Top.png" alt="nav_top">
		                            </a>
		                        </li>
	                        </ul>
				        </div>
			        </div>

			        <div class="row text-center">
			            <div class="footer-content">
			                <div class="col-md-4">
			                    <span>
			                        <img src="/images/Icon_Address.png" alt="icon_address">
			                        <p>No. 64 Camia corner, Sampaguita St. Lourdes Subd. Brgy. Mambugan Antipolo City 1870</p>
			                    </span>
			                </div>
			                <div class="col-md-4">
			                    <span>
			                        <img src="/images/Icon_Phone.png" alt="icon_phone">
			                        <p>647-9410 / 647-4374 (telefax)</p>
			                    </span>
			                </div>
			                <div class="col-md-4">
			                    <span>
			                        <img src="/images/Icon_Email.png" alt="icon_email">
			                        <p>inquire@learnedpublishing.com</p>
			                    </span>
			                </div>
			            </div>
			        </div>
					<div class="row text-center">
						<div class="footer-content">
							<div class="col-md-4 col-md-offset-4">
								<span>
									<a href="https://www.facebook.com/learnedpublishing" target="_blank">
										<img src="/images/Icon_Facebook.png" alt="icon_facebook">
									</a>
									<strong>Facebook</strong>
									<p>
										Copyright &copy; Learned Publishing <?php echo date('Y'); ?>
									</p>
								</span>
							</div>
						</div>
					</div>
				</footer>
			</div>
		</div>
	</body>
</html>