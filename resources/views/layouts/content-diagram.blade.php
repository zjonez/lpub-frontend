<div class="modal fade" id="contentDiagram" tabindex="-1"
     role="dialog" aria-labelledby="contentDiagram">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <div class="row-fluid image">
                    <img ng-src="/images/content-diagram.png" alt="Image" class="img-responsive"/>
                </div>
            </div>
        </div>
    </div>
</div>