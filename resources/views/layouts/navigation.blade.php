<li class="{{ Request::is('/') ? 'active' : '' }}">
    <a href="/">
        <strong>Home</strong>
    </a>
</li>
<li class="{{ Request::is('about-us') ? 'dropdown active' : 'dropdown' }}">
    <a href="/about-us"><strong>About Us</strong></a>
    <a href="javascript:void(0);" class="dropdown-toggle" data-toggle="dropdown"
       role="button" aria-haspopup="true" aria-expanded="false">
        <span class="caret arrow"></span>
    </a>
    <ul class="dropdown-menu">
        <li class="{{ Request::is('authors') ? 'active' : '' }}">
            <a href="/authors"><strong>Authors</strong></a>
        </li>
    </ul>
</li>
<li class="{{ Request::is('products') ? 'active' : '' }}">
    <a href="/products">
        <strong>Products</strong>
    </a>
</li>
<li class="{{ Request::is('services') ? 'active' : '' }}">
    <a href="/services">
        <strong>Services</strong>
    </a>
</li>
<li class="{{ Request::is('news') ? 'active' : '' }}">
    <a href="/news">
        <strong>News</strong>
    </a>
</li>
<li class="{{ Request::is('contact-us') ? 'active' : '' }}">
    <a href="/contact-us">
        <strong>Contact Us</strong>
    </a>
</li>

{{--
@if(Auth::check())
    <li>
        <!--@TODO: admin link redirect to dashboard-->
        <a href="javascript:void(0);"><strong>Admin</strong></a>
        <a href="javascript:void(0);" class="admin-dropdown-toggle" data-toggle="dropdown"
           role="button" aria-haspopup="true" aria-expanded="false">
            <span class="caret arrow"></span>
        </a>
        <ul class="dropdown-menu text-center">
            <li>
                <a href="#"><strong>Home</strong></a>
            </li>
            <li>
                <a href="#"><strong>About Us</strong></a>
            </li>
            <li>
                <a href="#"><strong>Products</strong></a>
            </li>
            <li>
                <a href="#"><strong>Services</strong></a>
            </li>
            <li>
                <a href="#"><strong>News</strong></a>
            </li>
        </ul>
    </li>
@endif
--}}