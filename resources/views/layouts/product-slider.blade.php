<!--loop through total pages-->
<div ng-repeat="book in books.items" class="row-fluid"> <!--products per page-->
    <div class="col-sm-4">
        <div class="row product">
            <div class="col-sm-6">
                <div class="row image">
                    <img ng-if="book.imageUrl" ng-src="<%book.imageUrl%>" alt="Image"
                         class="img-responsive"/>
                </div>
            </div>
            <div class="col-sm-6 details">
                <p class="grade" ng-bind="book.grade | gradeLabel | uppercase"></p>
                <ul> <!--@TODO: add editor here-->
                    <li><strong>Pages: </strong> <%book.numberOfPages%></li>
                    <li><strong>Copyright: </strong> <%book.copyright%></li>
                    <li ng-if="book.mainAuthor"><strong>Author: </strong> <%book.mainAuthor%></li>
                    <li ng-if="!book.mainAuthor">&nbsp;</li>
                    <li ng-if="!book.mainAuthor">&nbsp;</li>
                </ul>
                <p class="view">
                    <a class="btn btn-default btn-orange" ng-href="/book/<%book.bookID%>">
                        View Book
                    </a>
                </p>
            </div>
        </div>
    </div>
</div>