<p class="text-blue"><strong>LATEST POSTS</strong></p>

<div ng-repeat="post in settings.latestPosts">
    <div class="row latest-post">
        <div class="col-md-4">
            <div class="row">
                <img ng-src="<%post.thumbnailURL%>" class="img-responsive">
            </div>
        </div>
        <div class="col-md-8">
            <p>
                <a class="text-grey"href="/news/<%post.postID%>"><strong><%post.title%></strong></a>
            </p>
            <p>
                <i class="fa fa-calendar text-blue" aria-hidden="true"></i>
                <span class="text-grey" ng-bind="post.created_at | myDateFormat:'LLLL dd, yyyy'"></span>
            </p>
            <p>
                <i class="fa fa-comment text-blue" aria-hidden="true"></i>
                <span class="text-grey">0</span>
            </p>
        </div>
    </div>
</div>