<p class="text-blue"><strong>CATEGORIES</strong></p>
<div class="row category-list">
    <ul>
        <li ng-repeat="category in settings.categories track by $index">
            - <a href="javascript:void(0)" class="text-grey">
                <strong><%category.name | uppercase%></strong>
            </a>
            <span class="pull-right text-grey">
                <strong><%category.postCount%></strong>
            </span>
        </li>
    </ul>
</div>