<div class="modal fade" id="rationale" tabindex="-1"
     role="dialog" aria-labelledby="rationale">
    <div class="modal-dialog" role="document">
        <div class="modal-content">
            <div class="modal-header">
                <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span
                            aria-hidden="true">&times;</span></button>
            </div>
            <div class="modal-body">
                <h4>Rationale</h4>
                <p>
                    From early times teachers have relied upon various forms of visual and auditory aids to supplement
                    classroom learning. Research studies attest to the significantly higher learning outcomes achieved
                    by students when using appropriate media integrated in the curriculum. Hands-on minds-on science
                    activities have been found to be motivating and contributing to realizing the objectives of science
                    education.
                </p>

                <p>
                    Students respond to information differently. Thus, it is often to our advantage as teachers to use
                    many different formats and modes to teach the subject matter of a lesson. This is why teachers
                    normally use some combination of lecture, text and hands-on laboratory for conveying information.
                </p>

                <p>
                    The advent of the K-12 program added a more significant strain on its implementation, particularly
                    on how to enhance the teaching and learning process. The main objective of the project is to train
                    teachers to be resourceful and creative in producing their own intervention materials that focus on
                    core scientific principles in life sciences, physical sciences, earth sciences, and space. But more
                    importantly, they are also encouraged to design enhancement activities to reach out to pupils and
                    allow them to do activity-based lessons designed with interactive devices to make science more
                    stimulating and easier to understand.
                </p>

                <p>
                    Since science education is the root of technological innovation and creativity, it is believed that
                    this area may be improved if we provide the proper stimulation to the proper assemblage at the
                    proper stage in time. This means that we should provide enlivening learning activities to students
                    at an early age, thus encouraging and instilling in them a sustainable passion for education. The
                    resultant, in the long run, would be a country rich with a motivated human resource capable of
                    contributing and implementing ideas to better the nation.
                </p>

                <h4>Executive Summary</h4>

                <p>
                    The need to train science teachers in the implementation and enhancement of the K-12 science
                    curriculum is a given reality. But no matter how much training had already been given by DepEd, the
                    learning process still needs to be improved. In our experience of years of training science teachers
                    and holding mobile exhibits, one common denominator that is missing in the science classroom is
                    INTEREST and ENTHUSIASM of pupils. Once these are lacking, pupils do not generally “learn” the way
                    teachers expect them to. Pupils need to have “hands-on” activities to excite them. And this is
                    precisely what exhibits and this training is all about.
                </p>

                <p>
                    Teachers are inadequate in both materials and skills in performing experiments in class. They often
                    resort to giving lectures and showing computer-based activities. Even in this modern age of advanced
                    technology, there is no substitute for classroom-based hands-on activities to provide real-life
                    excitement among pupils.
                </p>

                <div class="pull-left">
                    <span class="art-signature">
                        <img src="images/art-signature.png" alt="art-signature" class="img-responsive" />
                    </span>

                    <ul class="art-details">
                        <li><h3>Arturo N. Villegas</h3></li>
                        <li><em>Proprietor of Scinergy Enterprises</em></li>
                        <li>Former Principal of Don Bosco Technical Institute (Tarlac)</li>
                    </ul>
                </div>
            </div>
        </div>
    </div>
</div>