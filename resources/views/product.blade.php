@extends('layouts.app')

@foreach($details as $data)

@section('header')
    <link href="/css/single-product.css" rel="stylesheet" type='text/css'/>
    <script src="/js/service/books.js"></script>
    <script src="/js/single-product.js"></script>
@endsection

@section('content')
        <div class="row-fluid single-product" ng-controller="singleProductController">
            <div class="container">
                <div class="row-fluid caption">
                    <h2 class="primary">PRODUCTS</h2>
                    <h1 class="secondary">BOOKS OF {{Str::upper($data->name)}}</h1>
                </div>

                <hr>

                <div class="row-fluid">
                    <div class="col-md-6">
                        <div class="row large-image">
                            <img src="{{$data->imageUrl}}" alt="Image" class="img-responsive" />
                        </div>
                    </div>
                    <div class="col-md-6">
                        <!-- @TODO: Temporarily this is the series description -->
                        <div class="row details">
                            <h4>PRODUCT DESCRIPTION</h4>
                            <p>
                                {!! $data->description !!}
                            </p>
                        </div>

                        <div class="row details">
                            <h4>TEXTBOOK SPECIFICATIONS</h4>
                            <table class="table table-condensed">
                                <tr>
                                    <td><strong>Size: </strong></td>
                                    <td>{{$data->size}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Pages: </strong></td>
                                    <td>{{$data->numberOfPages}} Pages</td>
                                </tr>
                                <tr>
                                    <td><strong>ISBN: </strong></td>
                                    <td>{{$data->ISBN}}</td>
                                </tr>
                                <tr>
                                    <td><strong>Year: </strong></td>
                                    <td>{{$data->copyright}} Edition</td>
                                </tr>
                                @if($data->authors)
                                    <tr>
                                        <td><strong>Authors: </strong></td>
                                        <td>{{$data->authors}}</td>
                                    </tr>
                                @endif
                            </table>
                        </div>

                        @if($data->hasContentDiagram)
                            <div class="row">
                                <a href='/images/content-diagram.png' target="_blank" class="btn btn-default btn-orange">
                                    View Content Diagram
                                </a>
                            </div>
                        @endif
                    </div>
                </div>
            </div>

            <div class="container recommended-books">
                <div class="row-fluid caption">
                    <h1 class="secondary"><strong><span class="text-blue">LEARN</span><span class="text-orange">ED</span></strong> BOOKS</h1>
                </div>

                <hr>

                <div class="row-fluid">
                    <div id="productsCarousel" class="carousel slide" ng-show="!productsLoading">
                        <div class="row">
                            <div class="pull-right">
                                <ol class="carousel-indicators">
                                    <li ng-repeat="page in bookPages" data-target="#productsCarousel" data-slide-to="<%page%>" ng-class="{active : $first}"></li>
                                    <li ng-show="!sliderStop" class="slider-options">
                                        <a ng-class="{'active-slider': !sliderStop}" ng-click="pauseSlider()">
                                            <span class="fa fa-pause"></span>
                                        </a>
                                    </li>
                                    <li ng-show="sliderStop" class="slider-options">
                                        <a ng-class="{'active-slider': sliderStop}" ng-click="playSlider()">
                                            <span class="fa fa-play"></span>
                                        </a>
                                    </li>
                                </ol>
                            </div>
                        </div>
                        <div class="carousel-inner">
                            <div ng-repeat="books in bookValues" class="item" ng-class="{active : $first}">
                                @include('layouts.product-slider')
                            </div>
                        </div>
                    </div>
                    <div us-spinner="{radius:30, width:8, length: 16}" class="lpub-spinner" spinner-key="spinner-1"></div>
                </div>
            </div>
            @include('layouts.content-diagram')
        </div>
@endsection

@endforeach