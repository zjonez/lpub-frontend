@extends('layouts.app')

@section('header')
    <link href="css/news.css" rel="stylesheet" type='text/css'/>
    <script src="js/service/post.js"></script>
    <script src="js/news.js"></script>
@endsection

@section('content')
    <div class="row-fluid news-lpub" ng-controller="newsController">
        <div class="container">
            <div class="row-fluid caption">
                <h2 class="primary">NEWS</h2>
                <h1 class="secondary">LATEST UPDATES</h1>
            </div>

            <div ng-if="settings.user" class="row-fluid">
                <div class="pull-right">
                    <a href="/new-post" class="btn btn-md btn-success">
                        <i class="fa fa-plus"></i> Add new
                    </a>
                </div>
            </div>
            
            <div class="row-fluid">
                <div class="col-md-8">
                    <div ng-repeat="post in posts" ng-show="!isLoading">
                        <div class="row post">
                            <h4 class="text-blue">
                                <strong><%post.title%></strong>
                            </h4>

                            <div class="row main-image">
                                <img ng-src="<%post.mainImage%>"  class="img-responsive"/>
                            </div>
                            <div class="details text-blue">
                                <span>
                                    <i class="fa fa-calendar" aria-hidden="true"></i>
                                    <span class="text-grey" ng-bind="post.created_at | myDateFormat:'LLLL dd, yyyy'"></span>
                                </span>
                                <span>
                                    <i class="fa fa-bookmark" aria-hidden="true"></i>
                                    <span class="text-grey" ng-bind="post.categoryName"></span>
                                </span>
                                <span>
                                    <i class="fa fa-comment" aria-hidden="true"></i>
                                    <span class="text-grey">0</span>
                                </span>
                                <span>
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                    <span class="text-grey">0</span>
                                </span>

                                <span class="pull-right text-grey">
                                    Posted By <span class="text-blue" ng-bind="post.username"></span>
                                </span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="content" ng-bind-html="post.content | sanitize">
                            </div>

                            <div class="row-fluid read-more">
                                <a href="/news/<%post.postID%>" class="btn btn-mini btn-orange">
                                    READ MORE
                                </a>
                            </div>

                            <hr>
                        </div>
                    </div>
                    <div class="row pagination" ng-show="count > 3">
                        <div class="input-group input-group-sm">
                            <span class="input-group-btn">
                                <button
                                        class="btn btn-mini btn-orange"
                                        ng-disabled="settings.page == 1"
                                        ng-click="settings.page = settings.page - 1; getPosts()">
                                    <i class="fa fa-arrow-circle-o-left" aria-hidden="true"></i>
                                    BACK
                                </button>
                            </span>
                            <input type="text" class="form-control input-xs" ng-model="settings.page" readonly/>
                            <span class="input-group-btn">
                                <button
                                        class="btn btn-mini btn-orange"
                                        ng-disabled="settings.page == pages.length"
                                        ng-click="settings.page = settings.page + 1; getPosts()">
                                    Next
                                    <i class="fa fa-arrow-circle-o-right" aria-hidden="true"></i>
                                </button>
                            </span>
                        </div>
                    </div>
                    <div us-spinner="{radius:30, width:8, length: 16}" class="lpub-spinner" spinner-key="spinner-1"></div>
                </div>
                <div class="col-md-4" ng-show="count != 0">
                    @include('layouts.categories')
                    @include('layouts.latest-posts')
                </div>
            </div>
        </div>
    </div>
@endsection