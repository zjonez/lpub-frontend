@extends('layouts.app')

@section('header')
    <link href="css/home.css" rel="stylesheet" type='text/css'>
@endsection

<!--@TODO: update UI -->
@section('content')
    {!! Form::open(['url' => 'login'])!!}
    <h1>Login</h1>

    <p>
        {!! $errors->first('email') !!}
        {!! $errors->first('password') !!}
    </p>

    <p>
        {!! Form::label('email', 'Email Address') !!}
        {!! Form::text('email', Input::old('email'), array('placeholder' => 'awesome@awesome.com')) !!}
    </p>

    <p>
        {!! Form::label('password', 'Password') !!}
        {!! Form::password('password') !!}
    </p>

    <p>{!! Form::submit('Submit!') !!}</p>
    {!! Form::close() !!}
@endsection