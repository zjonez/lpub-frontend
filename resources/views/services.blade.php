@extends('layouts.app')

@section('header')
    <link href="css/services.css" rel="stylesheet" type='text/css' xmlns="http://www.w3.org/1999/html">
    <script src="js/service/instruments.js"></script>
    <script src="js/service/images.js"></script>
    <script src="js/services.js"></script>
@endsection

@section('content')
    <div class="row-fluid services-lpub" ng-controller="servicesController">
        <div class="container">
            <div class="row-fluid caption">
                <h2 class="primary">SERVICES</h2>
                <h1 class="secondary">THE APPLICATION OF SCIENCE</h1>
            </div>
        </div>

        <div class="row main-image">
            <img src="/images/services-main.jpg" alt="Image" class="img-responsive" />
        </div>

        <!-- ABOUT SCINERGY -->
        <!-- @TODO: this is the actual static content -->
        <div class="container row-fluid about">
            <div class="col-md-4">
                <div class="row scinergy-logo">
                    <img src="/images/scinergy-logo.jpg" alt="Image" class="img-responsive" />
                </div>
            </div>
            <div class="col-md-8">
                <div class="row">
                    <h4 class="text-blue">ABOUT SCINERGY ENTERPRISES</h4>
                    <p>
                        <strong>Scinergy Enterprises,</strong> offers a training workshop for science teachers, entitled
                        "Enhancing Science Education Through Instrumentation". Its purpose is to give teachers the
                        appropriate knowledge and skills to make the teaching of science more <strong>activity based</strong>
                        by using improvised science instruments, which will improve the students'
                        understanding of the basic concepts in science(through observation and inference).
                        <a data-toggle="modal" class="rationale-link" data-target="#rationale">
                            <strong>(View Rationale)</strong>
                        </a>
                    </p>
                </div>

                <div class="row gallery">
                    <div id="galleryCarousel" class="carousel slide" ng-show="!galleryLoading">
                        <div class="carousel-inner">
                            <div ng-repeat="value in galleryValues" class="item" ng-class="{active : $first}">
                                <div ng-repeat="item in value.items" class="row-fluid">
                                    <div class="col-sm-6">
                                        <div class="row image">
                                            <a ng-click="openGalleryLightbox(item.imageUrl)">
                                                <img ng-if="item.thumbnailUrl" ng-src="<%item.thumbnailUrl%>" alt="Image" class="img-responsive" />
                                                <img ng-if="!item.thumbnailUrl" ng-src="<%item.imageUrl%>" alt="Image" class="img-responsive" />
                                            </a>
                                        </div>
                                        <div class="row caption">
                                            <em><span ng-bind="item.caption"></span></em>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div us-spinner="{radius:30, width:8, length: 16}" class="lpub-spinner" spinner-key="gallery-spinner"></div>
                </div>
            </div>
        </div>

        <div class="container instruments">
            <div class="row-fluid caption">
                <h2 class="primary">SERVICES</h2>
                <h1 class="secondary">IMPROVISED SCIENCE INSTRUMENTS</h1>
            </div>
            <div class="row-fluid">
                <div id="instrumentsCarousel" class="carousel slide" data-ride="carousel" ng-show="!isLoading">
                    <div class="row">
                        <div class="pull-right">
                            <ol class="carousel-indicators">
                                <li ng-repeat="page in pages" data-target="#instrumentsCarousel" data-slide-to="<%page%>" ng-class="{active : $first}"></li>
                                <li ng-show="!sliderStop" class="slider-options">
                                    <a ng-class="{'active-slider': !sliderStop}" ng-click="pauseSlider()">
                                        <span class="fa fa-pause"></span>
                                    </a>
                                </li>
                                <li ng-show="sliderStop" class="slider-options">
                                    <a ng-class="{'active-slider': sliderStop}" ng-click="playSlider()">
                                        <span class="fa fa-play"></span>
                                    </a>
                                </li>
                            </ol>
                        </div>
                    </div>

                    <div class="carousel-inner" role="listbox">
                        <div ng-repeat="value in values" class="item" ng-class="{active : $first}">
                            <!--loop through total pages-->
                            <div ng-repeat="item in value.items" class="row-fluid"> <!--products per page-->
                                <div class="col-sm-6">
                                    <div class="row product">
                                        <div class="col-md-4">
                                            <img ng-src="<%item.imageUrl%>" alt="Image" class="img-responsive"/>
                                        </div>
                                        <div class="col-md-8">
                                            <p class="grade" ng-bind="item.model"></p>
                                            <p class="title" ng-bind="item.name"></p>
                                            <p class="desc" ng-bind-html="item.description | ellipsis:100 | sanitize"></p>
                                            <p class="details">
                                                <a class="btn btn-default btn-blue" ng-click="openLightboxModal(item.model)">
                                                    MORE DETAILS
                                                </a>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div us-spinner="{radius:30, width:8, length: 16}" class="lpub-spinner" spinner-key="spinner-1"></div>
            </div>
        </div>

        <div class="container">
            <div class="row-fluid caption">
                <h2 class="primary">IMPROVISED SCIENCE INSTRUMENTS</h2>
                <h1 class="secondary">UNIVERSAL KIT</h1>
            </div>

            <div class="row-fluid">
                <p>
                    The universal kit is a box containing a common base and stand and all auxiliary stands and parts
                    needed to construct or assemble fifty (50) science instruments . It is the more affordable
                    counterpart of the set of forty (40) standalone instruments (refer to the instruments shown above),
                    or instruments with bases and stands, and ten (10) devices needing no bases and stands.
                </p>
            </div>
        </div>

        <div class="container">
            <div class="row-fluid caption">
                <h2 class="primary">IMPROVISED SCIENCE INSTRUMENTS</h2>
                <h1 class="secondary">STARTER KIT</h1>
            </div>

            <div class="row-fluid">
                <p>
                    The starter kit is an even more affordable substitute for the universal kit: It contains parts
                    needed for thirty eight (38) instruments.
                </p>
            </div>
        </div>
        @include('rationale')
    </div>
@endsection