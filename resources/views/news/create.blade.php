@extends('layouts.app')
@section('header')
    <link href="css/news.css" rel="stylesheet" type='text/css'/>
    <script src="js/service/post.js"></script>
@endsection

@section('content')
    <div class="row-fluid">
        <div class="container">
            <div class="row-fluid caption">
                <p class="primary">NEWS</p>
                <p class="secondary">ADD POST</p>
            </div>

            <div class="row-fluid">
                <form class="form-horizontal">
                    <div class="form-group">
                        <label class="col-sm-3 control-label">Title</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control" placeholder="title">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Permalink (this is auto-generated)</label>
                        <div class="col-sm-9">
                            <input type="text" class="form-control">
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-3 control-label">Category</label>
                        <div class="col-sm-9">
                            <select>
                                <option>Training</option>
                                <option>News</option>
                                <option>Category</option>
                            </select>
                        </div>
                    </div>
                </form>
            </div>
        </div>
    </div>
@endsection