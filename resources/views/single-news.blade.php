@extends('layouts.app')

@section('header')
    <link href="/css/news.css" rel="stylesheet" type='text/css'/>
    <script src="/js/service/post.js"></script>
    <script src="/js/single-news.js"></script>
@endsection

@section('content')
    <div class="row-fluid news-lpub" ng-controller="singleNewsController">
        <div class="container">
            <div class="row-fluid caption">
                <p class="primary">NEWS</p>
                <p class="secondary">LATEST UPDATES</p>
            </div>

            <div class="row-fluid">
                <div class="col-md-8">
                    @foreach($details as $data)
                        <div class="row post">
                            <h4 class="text-blue">
                                <strong>{{$data->title}}</strong>
                            </h4>

                            <div class="row main-image">
                                <img ng-src="{{$data->mainImage}}" class="img-responsive"/>
                            </div>
                            <div class="details text-blue">
                            <span>
                                <i class="fa fa-calendar" aria-hidden="true"></i>
                                <span class="text-grey">
                                    {{ Carbon\Carbon::parse($data->created_at)->format('F n, Y') }}
                                </span>
                            </span>
                            <span>
                                    <i class="fa fa-bookmark" aria-hidden="true"></i>
                                    <span class="text-grey">{{$data->categoryName}}</span>
                            </span>
                            <span>
                                    <i class="fa fa-comment" aria-hidden="true"></i>
                                    <span class="text-grey">0</span>
                            </span>
                            <span>
                                    <i class="fa fa-share-alt" aria-hidden="true"></i>
                                    <span class="text-grey">0</span>
                            </span>

                                <span class="pull-right text-grey">
                                    Posted By <span class="text-blue">{{$data->username}}</span>
                            </span>
                            </div>
                            <div class="clearfix"></div>
                            <div class="full-content">
                                {!! $data->content !!}
                            </div>
                            <hr>
                        </div>
                    @endforeach
                </div>
                <div class="col-md-4">
                    @include('layouts.categories')
                    @include('layouts.latest-posts')
                </div>
            </div>
        </div>
    </div>
@endsection