@extends('layouts.app')

@section('header')
    <link href="css/about.css" rel="stylesheet" type='text/css'/>
    <script src="js/service/authors.js"></script>
    <script src="js/about.js"></script>
@endsection

@section('content')
    <div class="row-fluid" ng-controller="aboutController">
        <div class="container about-lpub">
            <div class="row-fluid caption">
                <h2 class="primary">ABOUT US</h2>
                <h1 class="secondary">WELCOME TO LEARNED PUBLISHING</h1>
            </div>
            <div class="row-fluid details">
                <div class="col-md-6">
                    <div class="row main-image">
                        <img src="/images/sample-about-v2.jpg" alt="Image" class="img-responsive" />
                    </div>
                </div>
                <div class="col-md-6">
                    <!--@TODO: this is static content-->
                    <div class="row desc">
                        <p>
                            <strong><span class="text-blue">Learn</span><span class="text-orange">Ed</span></strong> was established in October 1, 2014 as a sole proprietorship. The name "LearnEd" came
                            from the English adjective <em>learned</em>, meaning one who possesses knowledge or experience. The
                            letter "E" is shown with a different color in our company logo (and also intentionally
                            capitalized in our business documents) to emphasize the intended meaning of the business
                            name. Learned specializes in Science textbooks and in the training of science teachers,
                            because it is obvious that science education, science appreciation, and the awareness of
                            science's relevance in human life is not among the common interests of Philippine society.
                            It is also because of this fact that the Philippines was never known to be one of the
                            leading countries in terms of science, scientific research, and technology. There are few
                            who are interested in careers in science. The company believes that in order for one to have
                            an interest and appreciation for science, he or she must first understand how science is
                            relevant to his or her own life, and how it has contributed to the progress of humanity.
                        </p>

                        <h4>History</h4>
                        <p>
                            Francisco Xavier L. Tupas, the proprietor of <span class="text-blue | uppercase"><strong>Academe Publishing House, Incorporated</strong></span>,
                            is the father of LearnEd's proprietor. Francisco Tupas was involved in projects related to
                            science education (environmental science) and awareness in the Philippines. He conducted
                            teacher training to mostly private schools, with his speakers coming from known agencies
                            such as the Philippine Institute of Volcanology and Seismology (PHIVOLCS). He created
                            <strong><em>"Science Club"</em></strong>, an informative and educational short science
                            magazine for students. Academe was one of the pioneers in distributing foreign (American)
                            quality textbooks to private schools in the Philippines during the 1980's. Francisco Tupas
                            died of a sudden heart attack in January 2002, leaving the operations of Academe to his
                            wife, Edna B. Tupas. Edna decided to pursue education at the Asian Institute of Management
                            (A.I.M.), since she did not have the experience required to manage the business. She
                            conducted numerous teacher trainings under Alberto Enrico Educational Center (a business
                            also owned by Academe), and helped organize the Association of Science Teachers and
                            Educators of The Philippines (A.S.T.E.P.). She was diagnosed with ovarian cancer in 2009,
                            and later died in October 2011. Academe was left with no one to manage it, and it eventually
                            fell into bankruptcy. Its operations ceased in 2014. After being sustained by Academe in his
                            lifetime, the eldest son of Mr. Tupas decided to establish a new textbook publishing
                            business, with a business name that is also related to education, like Academe.
                        </p>
                    </div>
                </div>
            </div>
        </div>


        <div class="container authors">
            <div class="row-fluid caption">
                <h1 class="secondary">AUTHORS</h1>
            </div>
            <div class="row-fluid">
                <div ng-repeat="author in authors" class="col-md-4 author" ng-show="!isLoading">
                    <div class="row-fluid image">
                        <img ng-src="<%author.imageUrl%>" alt="Image"
                             class="img-responsive" />
                    </div>
                    <div class="row-fluid">
                        <p class="name text-center text-blue" ng-bind="author | authorName | uppercase"></p>
                        <p class="desc" ng-bind-html="author.description | ellipsis:225 | sanitize"></p>
                        <p class="text-center">
                            <a href="/authors/#<%author.lastName | lowercase%>" class="btn btn-mini btn-orange">
                                MORE INFO
                            </a>
                        </p>
                    </div>
                </div>

                <div us-spinner="{radius:30, width:8, length: 16}" class="lpub-spinner" spinner-key="spinner-1"></div>
            </div>
        </div>

        <div class="container partners">
            <div class="row-fluid caption">
                <h1 class="secondary">PARTNERS</h1>
            </div>

            <div class="row-fluid">
                <div class="col-md-12">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="row partner">
                                <a href="http://www.wealthlink.org/" target="_blank">
                                    <img src="/images/uploads/partners/wealthlink.jpg" alt="Image" class="img-responsive" />
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="row partner">
                                <a href="https://www.facebook.com/academepublishinghouse" target="_blank">
                                    <img src="/images/uploads/partners/academev3.jpg" alt="Image" class="img-responsive" />
                                </a>
                            </div>
                        </div>
                        <div class="col-md-4">
                            <div class="row partner">
                                <a href="http://www.pereznumedia.com/" target="_blank">
                                    <img src="/images/uploads/partners/pnm-logo.png" alt="Image" class="img-responsive" />
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
@endsection